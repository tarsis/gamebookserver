@extends('layout')
@section('content')

	<div class="row" id="app">
		<div class="col-12 justify-content-center">

			<div class="card" style="margin-top: 60px;">
				<div class="card-body">
					<h5 class="card-title">Cadastro</h5>
					{{--<p class="card-text">Listar</p>--}}

			{{--		<form method="POST">

						<div class="form-group">
							<label for="passaporte">FULL</label>
							<input type="passaporte" class="form-control" id="passaporte" placeholder="FILTRO">
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>
--}}
					<lista :players="{{json_encode($players)}}" group="RO"></lista>

				</div>

			</div>


		</div>

	</div>

	{{--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>--}}
	<script src="vue.js"></script>
	<script src="js/axios.min.js"></script>

	<script type="text/x-template" id="lista">
		<div>
		TOTAL ((players.length)) Group ((groupPlayers.length)) <br>

			<div class="col-12">
				<table width="100%">
					<tbody>
					<tr>
						<th>Nick</th>
						<td><input type="text" v-model="form.player_name" disabled><br>
							<h5 class="float-left"><span v-for="player in uniqueNames" class="float-left badge badge-primary" @click="form.player_name= player">((player))</span></h5>
						</td>
						<th>Nome completo</th>
						<td><input type="text" v-model="form.full_name">
						</td>
					</tr>

					<tr>
						<th>Idade</th>
						<td><input type="text" v-model="form.age" >
							<br>
							<h5 ><span v-for="age in uniqueAges" class="float-left badge badge-primary" @click="form.age= age">((age))</span></h5>
						</td>
						<th>Grupo</th>
						<td>
							<select v-model="form.entity">
								<option :value="ent" v-for="ent in listEntities">((ent))</option>
							</select>
						{{--	<input type="text" v-model="form.entity" disabled>--}}
						</td>
						<br>
						<h5 ><span v-for="entity in uniqueEntities" class="float-left badge badge-primary" @click="form.entity= entity">((entity))</span></h5>

					</tr>

					</tbody>
				</table>
				<input type="text" v-model="form.obs"> <a href="#" class="btn btn-primary"  @click.prevent="salvar()">Salvar</a>
			</div>
			<br>

		<table class="table table-bordered col-8" >
			<thead>
				<tr>
					<th></th>
					<th>Nink</th>
					<th>Idade</th>
					<th>Grupo</th>

				</tr>
			</thead>
			<tbody>
			<tr v-for="(item, key) in players">
				<td><input type="checkbox" v-model="selecteds" :value="item"></td>
				<td><a  @click="ok(item)">(( item.player_name ))</a></td>
				<td>(( item.age ))</td>
				<td>(( item.entity ))</td>
				{{--<td><a href="" class="btn-active btn-sm">Mesclar</a></td>--}}
			</tr>
			</tbody>

		</table>

			<div id="footerx">

			</div>
	</div>
	</script>


	<script type="text/javascript">
        Vue.config.delimiters = ['((', '))'];
        Vue.component('lista', {
            delimiters: ["((","))"],
            props:[
                'players',
				'group'
            ],
            created:function() {
                this.items = this.players;
            },
			watch:{
                'selectedPlayers' : function (newVal) {
					if(newVal.length > 0) {
					    this.form.id = newVal[0].id;
					    this.form.player_name = newVal[0].player_name;
					    this.form.age = newVal[0].age;
					    this.form.entity = newVal[0].entity;
					} else {
                        this.form.id = '';
                        this.form.player_name = '';
                        this.form.age = '';
                        this.form.entity = '';
                        this.form.id_owner = null;
                        this.form.obs = null;
					}
                }
			},
			data:function(){
                return {
                    items: [],
                    selecteds:[],
					form: {
                        id: '',
						full_name: '',
						player_name: '',
						age: '',
						entity: '',
						id_owner:null,
						obs: null
					}
				}
			},
			methods:{
			    ok:function(item){
			        if(this.selecteds.indexOf(item) == -1){
                        this.selecteds.push(item);
                        this.form.obs = 'OK';
                        this.$nextTick( () => this.salvar() );

					}
				},
			    salvar:function () {
			        this.form.selected = this.selecteds.map(function (item) { return item.id });
					axios.post('/salvar', this.form).then(function(r){
					    if(r.data.success){
                            this.selecteds = [];
						}
					    window.location = window.location.href;
					}).catch(function(r){
                        alert('erro '.r.data.msg);
					});
                },
			    uniques:function(key){
					var uniques = [];
					this.selecteds.map( function (value, index) {
						if(uniques.indexOf(value[key]) == -1) {
							uniques.push (value[key]);
						}
					});
					return uniques;
        		},
			},
			computed:{
                groupPlayers: function() {
                    return this.players.filter(function(item){
                        return item.entity == this.group;
                    });
                },
                list: function() {
                    return this.players.filter(function(item){
                        return !item.owner_id ;
                    });
                },

                selectedPlayers: function(){
                   return this.selecteds
                        .sort(function (a, b) {
                           return b.age - a.age; // desc
                        })
                },
				uniqueNames:function(){
                    return this.uniques('player_name');
                    var uniques = [];
                    this.selecteds.map( function (value, index) {
                        if(uniques.indexOf(value.player_name) == -1) {
                            uniques.push (value.player_name);
						}
                    });
                    return uniques;
				},
                uniqueAges:function(){
                    return this.uniques('age');
				},
				uniqueEntities:function(){
                    return this.uniques('entity');
				},
				listEntities:function(){
                    var uniques = [];
                    this.players.map( function (value, index) {
                        if(uniques.indexOf(value['entity']) == -1) {
                            uniques.push (value['entity']);
                        }
                    });
                    return uniques;
				}


            },
            template:'#lista'
        });

        var app = new Vue({
            el: '#app',
            delimiters: ["((","))"],
            data: {
                message: 'Hello Vue!'
            }
        })

	</script>
	<style>
		#footer{
			position: fixed; border: 1px solid #ccc; height: 200px;width:100%;bottom: 30px;background: #efefef;
			margin:0px auto;
			padding:20px;
		}
		.badge{
			margin-right:10px; cursor:pointer;
			min-width: 10px;
		}
	</style>
@stop
