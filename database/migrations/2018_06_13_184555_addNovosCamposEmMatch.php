<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNovosCamposEmMatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         throw new Exception('x');

        Schema::create('match_hits', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('match_id');
            $table->float('response_time_avg')->nullable()->before('updated_at');
            $table->integer('hits_0_2')->nullable();
            $table->integer('hits_2_4')->nullable();

            $table->integer('hits_0_4')->nullable();
            $table->integer('hits_4_8')->nullable();
            $table->integer('hits_8_12')->nullable();
            $table->integer('hits_12_16')->nullable();
            $table->integer('hits_16_20')->nullable();
            $table->integer('hits_20_24')->nullable();
            $table->integer('hits_24_28')->nullable();
            $table->integer('hits_28_32')->nullable();
            $table->integer('hits_32_36')->nullable();
            $table->integer('hits_36_40')->nullable();
            $table->integer('hits_40_mais')->nullable();


            $table->integer('errors_0_2')->nullable();
            $table->integer('errors_2_4')->nullable();

            $table->integer('errors_0_4')->nullable();
            $table->integer('errors_4_8')->nullable();
            $table->integer('errors_8_12')->nullable();
            $table->integer('errors_12_16')->nullable();
            $table->integer('errors_16_20')->nullable();
            $table->integer('errors_20_24')->nullable();
            $table->integer('errors_24_28')->nullable();
            $table->integer('errors_28_32')->nullable();
            $table->integer('errors_32_36')->nullable();
            $table->integer('errors_36_40')->nullable();
            $table->integer('errors_40_mais')->nullable();

            $table->integer('omissions_0_2')->nullable();
            $table->integer('omissions_2_4')->nullable();

            $table->integer('omissions_0_4')->nullable();
            $table->integer('omissions_4_8')->nullable();
            $table->integer('omissions_8_12')->nullable();
            $table->integer('omissions_12_16')->nullable();
            $table->integer('omissions_16_20')->nullable();
            $table->integer('omissions_20_24')->nullable();
            $table->integer('omissions_24_28')->nullable();
            $table->integer('omissions_28_32')->nullable();
            $table->integer('omissions_32_36')->nullable();
            $table->integer('omissions_36_40')->nullable();
            $table->integer('omissions_40_mais')->nullable();

            $table->integer('response_time_0_2')->nullable();
            $table->integer('response_time_2_4')->nullable();

            $table->integer('response_time_0_4')->nullable();
            $table->integer('response_time_4_8')->nullable();
            $table->integer('response_time_8_12')->nullable();
            $table->integer('response_time_12_16')->nullable();
            $table->integer('response_time_16_20')->nullable();
            $table->integer('response_time_20_24')->nullable();
            $table->integer('response_time_24_28')->nullable();
            $table->integer('response_time_28_32')->nullable();
            $table->integer('response_time_32_36')->nullable();
            $table->integer('response_time_36_40')->nullable();
            $table->integer('response_time_40_mais')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
