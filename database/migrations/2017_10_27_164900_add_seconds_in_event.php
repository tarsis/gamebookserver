<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddSecondsInEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        return true;
        DB::statement('ALTER TABLE `event` CHANGE `datetime` `datetime` DATETIME(3) NOT NULL;');

        Schema::table('event', function(Blueprint $table)
        {
            $table->decimal('second', 3, 4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
