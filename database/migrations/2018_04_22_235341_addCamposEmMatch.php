<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposEmMatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('match', function(Blueprint $table)
        {
            $table->integer('match_level_order')->nullable()->after('match_order')->change();
            //$table->integer('match_level_order_g')->nullable()->after('match_order')->change();
            //$table->integer('clicks')->nullable();
        });


        return;

        Schema::table('match', function(Blueprint $table)
        {
            $table->integer('match_order')->nullable()->after('id');
            $table->integer('total_targets')->nullable()->after('omissions');
            $table->double('first_reaction')->nullable()->after('duration');
            $table->double('match_level_order')->nullable()->after('first_reaction');
            $table->integer('player_old')->nullable()->after('player_id');
            $table->string('match_result')->nullable();
            //$table->integer('clicks')->nullable();
        });

        Schema::table('player', function(Blueprint $table)
        {
            $table->string('gender')->nullable();
        });

        Schema::table('event', function(Blueprint $table)
        {
            $table->string('object_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('match', function(Blueprint $table)
        {
            $table->integer('match_level_order')->nullable()->after('match_order')->change();
            //$table->integer('clicks')->nullable();
        });
    }
}
