<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMatchjson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      return true;
        Schema::create('matchjson', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('sessionId')->unique();
            $table->integer('playerId');
            $table->integer('age');
            $table->longText('payload');
            $table->dateTime('datetime');
            $table->text('obs')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('matchjson');
    }
}
