<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelaNomeJogador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        return;
        Schema::create('player_names', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('player_id');
            $table->integer('original_id');
            $table->string('group')->nullable();
            $table->string('name')->nullable();
            $table->integer('age')->nullable();
            $table->string('full_name')->nullable();
            $table->integer('excluir')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
