CREATE ALGORITHM=UNDEFINED DEFINER=`sagmcv`@`%%` SQL SECURITY DEFINER VIEW `view_eventos_partida`  AS  select `e`.`match_id` AS `partida`,`e`.`category` AS `categoria`,`event_type`.`name` AS `tipo_evento`,`e`.`name` AS `descricao`,`e`.`click_target` AS `alvo_clicado`,`e`.`drop_target` AS `alvo_arrastado_para`,`e`.`created_at` AS `criado_em` from (`event` `e` left join `event_type` on((`event_type`.`id` = `e`.`type`))) order by `e`.`created_at`,`e`.`id` ;

CREATE ALGORITHM=UNDEFINED DEFINER=`sagmcv`@`%%` SQL SECURITY DEFINER VIEW `view_partidas`  AS  select `p`.`player_name` AS `jogador`,`mg`.`name` AS `minigame`,`m`.`hits` AS `acertos`,`m`.`errors` AS `erros`,`m`.`repeated_errors` AS `erros repetidos`,`m`.`omissions` AS `omissoes`,`m`.`duration` AS `duracao`,`m`.`created_at` AS `criado_em` from ((`match` `m` join `player` `p` on((`m`.`player_id` = `p`.`id`))) join `minigame` `mg` on((`m`.`minigame_id` = `mg`.`id`))) order by `m`.`created_at`,`m`.`id` ;

# atualiza dados que vieram cortados
select * from `bkp`.`import` where `bkp`.`import`.`extracted` = 0 and `bkp`.`import`.`id` in (SELECT `starter`.`import`.`id` FROM `starter`.`import` WHERE length(`starter`.`import`.`payload` ) =65535)


CREATE VIEW `mi6ni10` AS
select `bkp`.`player`.`id` AS `id`,`bkp`.`player`.`player_name` AS `player_name`,`bkp`.`player`.`age` AS `age`,`bkp`.`player`.`entity` AS `entity`,`bkp`.`player`.`excluir` AS `excluir`,`bkp`.`player`.`obs` AS `obs`,`bkp`.`player`.`created_at` AS `created_at`,`bkp`.`player`.`updated_at` AS `updated_at`,`bkp`.`player`.`full_name` AS `full_name`,`bkp`.`player`.`id_owner` AS `id_owner`,`bkp`.`player`.`gender` AS `gender` from `bkp`.`player`

where (
  isnull(`bkp`.`player`.`excluir`)

    and `bkp`.`player`.`id` in (
        select `bkp`.`match`.`player_id` from `bkp`.`match`
          where ((`bkp`.`match`.`level_id` = 10)
          and (`bkp`.`match`.`minigame_id` = 6))
    )
)


CREATE
    ALGORITHM = UNDEFINED
    DEFINER = `root`@`%`
    SQL SECURITY DEFINER
VIEW `minigame_6` AS
    SELECT
        `match`.`id` AS `id`,
        `player`.`player_name` AS `player_name`,
        `player`.`age` AS `player_age`,
        `player`.`gender` AS `player_gender`,
        `player`.`entity` AS `player_entity`,
        `match`.`match_order` AS `match_order`,
        `match`.`match_level_order` AS `match_level_order`,
        `match`.`minigame_id` AS `minigame_id`,
        `match`.`level_id` AS `level_id`,
        `match`.`datetime` AS `datetime`,
        `match`.`hits` AS `hits`,
        `match`.`errors` AS `errors`,
        `match`.`repeated_errors` AS `repeated_errors`,
        `match`.`aleatory_actions` AS `aleatory_actions`,
        `match`.`omissions` AS `omissions`,
        `match`.`total_targets` AS `total_targets`,
        `match`.`duration` AS `duration`,
        `match`.`match_result` AS `match_result`
    FROM
        (`match`
        JOIN `player` ON ((`player`.`id` = `match`.`player_id`)))
    WHERE
        ((`match`.`minigame_id` = 6)
            AND ISNULL(`match`.`obs`)
            AND `match`.`player_id` IN (SELECT
                `mi6n_1_a_6`.`id`
            FROM
                `mi6n_1_a_6`))
    ORDER BY `match`.`datetime`


CREATE
    ALGORITHM = UNDEFINED
    DEFINER = `root`@`%`
    SQL SECURITY DEFINER
VIEW `mi6n_1_a_6` AS
    SELECT
        `player`.`id` AS `id`
    FROM
        `player`
    WHERE
        (ISNULL(`player`.`excluir`)
            AND (`player`.`entity` NOT IN ('CV' , 'TE', 'DDE'))
            AND (`player`.`age` > 0)
            AND `player`.`id` IN (SELECT
                `mi6ni1`.`id`
            FROM
                `mi6ni1`)
            AND `player`.`id` IN (SELECT
                `mi6ni2`.`id`
            FROM
                `mi6ni2`)
            AND `player`.`id` IN (SELECT
                `mi6ni3`.`id`
            FROM
                `mi6ni3`)
            AND `player`.`id` IN (SELECT
                `mi6ni4`.`id`
            FROM
                `mi6ni4`)
            AND `player`.`id` IN (SELECT
                `mi6ni5`.`id`
            FROM
                `mi6ni5`)
            AND `player`.`id` IN (SELECT
                `mi6ni6`.`id`
            FROM
                `mi6ni6`))

# query para pegar eventos por segundo
select match_id, type,  second_int, count(type) from `event`
where
match_id in (select `match`.`id` from `match` where minigame_id= 6) and
second_int is not null and
type = 1
#and second_int =2
group by match_id, type, second_int order by second_int desc