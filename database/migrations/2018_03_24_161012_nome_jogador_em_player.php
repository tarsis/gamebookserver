<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NomeJogadorEmPlayer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player', function(Blueprint $table)
        {
            $table->string('full_name')->nullable();
            $table->integer('id_owner')->nullable();
        });

        Schema::table('player_bkp', function(Blueprint $table)
        {
            $table->string('full_name')->nullable();
            $table->integer('id_owner')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
