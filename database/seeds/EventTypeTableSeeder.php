<?php

use Illuminate\Database\Seeder;

class EventTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::table('event_type')->delete();

        \DB::table('event_type')->insert(array(
            1 => array(
                'id' => 1,
                'name' => 'HIT',
                'description' => 'Acerto'
            ),
            2 => array(
                'id' => 2,
                'name' => 'ERROR',
                'description' => 'Erro'
            ),
            3 => array(
                'id' => 3,
                'name' => 'ALEATORY_ACTION',
                'description' => 'Açao aleatoria'
            ),

            4 => array(
                'id' => 4,
                'name' => 'MOVE',
                'description' => 'Movimento'
            ),
            10 => array(
                'id' => 10,
                'name' => 'GAME_START',
                'description' => 'Inicio de jogo'
            ),
            11 => array(
                'id' => 11,
                'name' => 'GAME_OMISSION',
                'description' => 'Possivel omisssao'
            ),
            12 => array(
                'id' => 12,
                'name' => 'GAME_FINISH',
                'description' => 'Fim de jogo'
            ),
            13 => array(
                'id' => 13,
                'name' => 'GAME_OVER',
                'description' => 'Perdeu a partida'
            ),
            14 => array(
                'id' => 14,
                'name' => 'GAME_PAUSE',
                'description' => 'Parada no jogo'
            ),
            15 => array(
                'id' => 15,
                'name' => 'GAME_RESUME',
                'description' => 'Retorna ao jogo apos pause'
            ),
            16 => array(
                'id' => 16,
                'name' => 'GAME_LEAVE',
                'description' => 'Saiu do jogo antes de terminar'
            ),
        ));
    }
}
