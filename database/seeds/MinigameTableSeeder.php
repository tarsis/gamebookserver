<?php

use Illuminate\Database\Seeder;

class MinigameTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::table('minigame')->delete();

        \DB::table('minigame')->insert(array (
            array(
                'id' => 1,
                'name' => 'Jogo da Vitória Régia',
                'description' => 'O jogador deve arrastar as vitorias regias para o espaço que tem o mesmo formato.',
                'game_number' => 1,
                'events_per_move' => '1',
                'perfect_move' => '1'
            ),
            array(
                'id' => 2,
                'name' => 'Jogo da gaiola',
                'description' => 'Após selecionar uma gaiola, o jogador deve apertar no botão quando a marcação ficar em cima da barra verde.
O processo deve ser feito para as três gaioloas antes que acabe o tempo.',
                'game_number' => 2,
                'events_per_move' => '1',
                'perfect_move' => '1'
            ),
            array(
                'id' => 4,
                'name' => 'Jogo do plantio',
                'description' => 'O jogador deve arrastar uma árvore para cada buraco respeitando a ordem estabelecida no jogo.
As primeiras jogadas acabam sendo por tentativa e erro até que se tenha memorizado qual a sequência correta.',
                'game_number' => 4,
                'events_per_move' => '1',
                'perfect_move' => '1'
            ),
            array(
                'id' => 6,
                'name' => 'Jogo da coruja',
                'description' => 'O Jogador deve clicar nos personagens mostrados no inicio e não clicar nos diferentes destes.',
                'game_number' =>6,
                'events_per_move' => '1',
                'perfect_move' => '1'
            ),
        ));
    }
}
