<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGbUsuarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('gb_usuario', function(Blueprint $table)
		{
			$table->foreign('ID_Responsavel', 'gb_usuario_ibfk_1')->references('ID_Responsavel')->on('gb_responsavel')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('gb_usuario', function(Blueprint $table)
		{
			$table->dropForeign('gb_usuario_ibfk_1');
		});
	}

}
