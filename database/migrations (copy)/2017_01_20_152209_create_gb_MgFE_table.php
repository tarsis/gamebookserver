<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGbMgFETable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gb_MgFE', function(Blueprint $table)
		{
			$table->integer('ID_Minigame')->index('gb_MgFE_ibfk_1');
			$table->integer('ID_FE')->index('gb_MgFE_ibfk_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gb_MgFE');
	}

}
