<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGbMinigameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('gb_minigame', function(Blueprint $table)
		{
			$table->foreign('ID_Usuario', 'gb_minigame_ibfk_1')->references('ID_Usuario')->on('gb_usuario')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('gb_minigame', function(Blueprint $table)
		{
			$table->dropForeign('gb_minigame_ibfk_1');
		});
	}

}
