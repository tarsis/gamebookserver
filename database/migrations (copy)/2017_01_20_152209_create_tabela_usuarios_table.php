<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTabelaUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tabela_usuarios', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('login', 100);
			$table->string('senha', 100);
			$table->string('Primeiro_nome', 100);
			$table->string('email', 100);
			$table->integer('quantidade')->default(2);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tabela_usuarios');
	}

}
