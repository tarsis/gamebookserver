<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGbResponsavelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gb_responsavel', function(Blueprint $table)
		{
			$table->integer('ID_Responsavel', true);
			$table->string('Nome')->nullable();
			$table->string('Instituicao')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gb_responsavel');
	}

}
