<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGbMinigameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gb_minigame', function(Blueprint $table)
		{
			$table->integer('ID_Minigame', true);
			$table->string('Nome')->nullable();
			$table->integer('Nivel')->nullable();
			$table->integer('Score')->nullable();
			$table->integer('Estrelas')->nullable();
			$table->integer('Max_Score')->nullable();
			$table->integer('Data')->nullable();
			$table->integer('Hora')->nullable();
			$table->integer('ID_Usuario')->nullable()->index('gb_minigame_ibfk_1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gb_minigame');
	}

}
