<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGbFETable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gb_FE', function(Blueprint $table)
		{
			$table->integer('ID_FE', true);
			$table->string('Nome')->nullable();
			$table->string('Codigo')->nullable();
			$table->integer('Valor')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gb_FE');
	}

}
