<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGbUsuarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gb_usuario', function(Blueprint $table)
		{
			$table->integer('ID_Usuario', true);
			$table->string('Nome')->nullable();
			$table->integer('Idade')->nullable();
			$table->string('Cidade')->nullable();
			$table->string('Pais')->nullable();
			$table->integer('ID_Responsavel')->nullable()->index('gb_usuario_ibfk_1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gb_usuario');
	}

}
