<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGbMgFETable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('gb_MgFE', function(Blueprint $table)
		{
			$table->foreign('ID_Minigame', 'gb_MgFE_ibfk_1')->references('ID_Minigame')->on('gb_minigame')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('ID_FE', 'gb_MgFE_ibfk_2')->references('ID_FE')->on('gb_FE')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('gb_MgFE', function(Blueprint $table)
		{
			$table->dropForeign('gb_MgFE_ibfk_1');
			$table->dropForeign('gb_MgFE_ibfk_2');
		});
	}

}
