<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGbEventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event', function(Blueprint $table)
		{
            $table->increments('id');
			$table->integer('match_id', false, true)->index('match_id');
			$table->integer('category');
			$table->integer('type');
			$table->string('name');
			$table->dateTime('datetime');
			$table->string('click_target', 100)->nullable()->comment('alvo do tipo clique');
			$table->string('drop_target', 100)->nullable()->comment('alvo do tipo arrastar-soltar ');
			$table->float('target_x', 10, 0)->nullable();
			$table->float('target_y', 10, 0)->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event');
	}

}
