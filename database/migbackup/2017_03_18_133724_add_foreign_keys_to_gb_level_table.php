<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGbLevelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('level', function(Blueprint $table)
		{
			$table->foreign('minigame_id','level_ibfk_1')->references('id')->on('minigame')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('level', function(Blueprint $table)
		{
			$table->dropForeign('level_ibfk_1');
		});
	}

}
