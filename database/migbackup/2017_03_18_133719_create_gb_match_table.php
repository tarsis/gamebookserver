<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGbMatchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match', function(Blueprint $table)
		{
            $table->increments('id');
			$table->string('game_session_id')->unique('game_session_id');
            $table->integer('player_id',false, true)->index('player_id');
            $table->integer('minigame_id',false, true)->index('minigame_id');
            $table->integer('level_id',false, true)->index('level_id');
			$table->dateTime('datetime');
			$table->integer('hits')->comment('quantidade de acertos');
			$table->integer('errors')->comment('quantidade de erros');
			$table->integer('repeated_errors')->comment('quantidade de erros repetidos');
			$table->integer('omissions')->comment('quantidade de omissoes');
            $table->integer('aleatory_actions')->default(0)->comment('cliques em não alvos');
			$table->integer('duration')->comment('duracao da partida em segundos');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match');
	}

}
