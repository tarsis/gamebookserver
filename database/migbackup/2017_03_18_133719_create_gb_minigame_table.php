<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGbMinigameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('minigame', function(Blueprint $table)
		{
            $table->increments('id');
			$table->string('name');
			$table->integer('game_number');
			$table->integer('events_per_move')->comment('nunero de eventos correspondente a uma jogada');
			$table->integer('perfect_move')->comment('numero minimo de jogadas para ganhar');
			$table->text('description', 65535)->comment('descricao sobre o game e funcoes executivas ');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('minigame');
	}

}
