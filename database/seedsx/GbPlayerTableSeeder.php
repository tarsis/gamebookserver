<?php

use Illuminate\Database\Seeder;

class GbPlayerTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('gb_player')->delete();
        
        \DB::table('gb_player')->insert(array (
            0 => 
            array (
                'id' => 1,
                'player_name' => 'bob',
                'age' => 6,
                'entity' => 'CV',
                'created_at' => '2017-03-18 16:52:54',
                'updated_at' => '2017-03-18 16:52:54',
            ),
            1 => 
            array (
                'id' => 2,
                'player_name' => 'cabecao w',
                'age' => 6,
                'entity' => 'CV',
                'created_at' => '2017-03-03 12:39:09',
                'updated_at' => '2017-03-18 16:53:53',
            ),
            2 => 
            array (
                'id' => 3,
                'player_name' => 'clinic',
                'age' => 0,
                'entity' => 'CV',
                'created_at' => '2017-03-18 16:56:38',
                'updated_at' => '2017-03-18 16:56:38',
            ),
        ));
        
        
    }
}