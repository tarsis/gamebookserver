<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Database tables.
     *
     * @var array
     */
    protected $tables = [
      /*  'users',
        'products',
        'categories',*/
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate();
        $this->call(GbMatchTableSeeder::class);
        $this->call(GbPlayerTableSeeder::class);
/*
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(LudIndicadoresTableSeeder::class);
        $this->call(LudAdministradoresTableSeeder::class);
        $this->call(LudBannersTableSeeder::class);
        $this->call(LudCidadesTableSeeder::class);
        $this->call(LudEstadosTableSeeder::class);
        $this->call(LudGenerosTableSeeder::class);
        $this->call(LudGruposTableSeeder::class);
        $this->call(LudGruposJogadoresTableSeeder::class);
        $this->call(LudInstituicoesTableSeeder::class);
        $this->call(LudJogadoresTableSeeder::class);
        $this->call(LudJogosTableSeeder::class);
        $this->call(LudNoticiasTableSeeder::class);
        $this->call(LudRegistrosTableSeeder::class);
        $this->call(LudUsuariosTableSeeder::class);
        $this->call(GamebookTableSeeder::class);*/
    }

    /**
     * Clear all tables in the database.
     *
     * @return void
     */
    protected function truncate()
    {
        Schema::disableForeignKeyConstraints();

        foreach ($this->tables as $name) {
            DB::table($name)->truncate();
        }

        Schema::enableForeignKeyConstraints();
    }
}
