<?php

use Illuminate\Foundation\Inspiring;

use App\Services\GamebookService;
use App\Services\EventService;
use App\Services\ExtractService;
use App\Models\Import;
use App\Models\GbMatch;
use App\Models\GbEvent;
use App\Models\Player as Player;
use App\MatchJson;

//use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

// #### fluxo #####
// extract
// fix-matchesjson-duplicado (ja é feito na extracao)
// extract-match
// atualiza-partidas-aptas  somente se aparecer novo marcado como apto
// nivel-maximo jogado
// gb:atualiza-qtd-eventos chama atualiza-partidas-aptas
// atualiza-lista-alvos incluir na checagem-qtd-eventos apos atualizar
// checa-qtd-eventos - ja chama o confere=match
            // confere-matchjson (verifica se faltam eventos e inclui-os)
// gb:corrige-falso-acerto
// verifica-contagens verifica se qtd eventos confere

// fix-seconds - prepara para os proximos calculos


Artisan::command('gb:tempo-medio-resposta', function (EventService $eventService) {
    $minigames = $eventService->minigamesAptos()->filter(function($item){
      return $item->tem_objetos ==1;
    });

    foreach($minigames as $minigame) {

        $events = $minigame->events()->get()->filter(function($item) {
            $eventosAnalisados = [
                GbEvent::TYPE_GAME_SHOWED_TARGET,
                GbEvent::TYPE_HIT,
                GbEvent::TYPE_ERROR,
                GbEvent::TYPE_GAME_OMISSION
            ];

            return in_array($item->type, $eventosAnalisados);
        });

        $omissoes = $events->groupBy('object_id')->filter(function($item){
            $this->info($item->count());
            if($item->count() == 2
                && $item[0]->type == GbEvent::TYPE_GAME_SHOWED_TARGET
            && $item[0]->name == 'Alvo apareceu' &&
            $item[1]->type == GbEvent::TYPE_GAME_OMISSION) {
                $item[1]->omissao = 1;
                $item[1]->save();

                return true;
            }

            return false;
        });

        $omissoes = $events->groupBy('object_id')->filter(function($item){
            $this->info($item->count());
            if($item->count() == 1
                && $item[0]->type == GbEvent::TYPE_GAME_OMISSION
                ) {
                //dd($item[0]->toArray());
                return true;
            }

            return false;
        });

        $minigame->omissions = $omissoes;
        $minigame->save();

//        if($omissoes->count() > 0) {
//            dd($omissoes->map(function($item){
//                return $item->toArray();
//            }));
//        }
        //dd($omissoes);
        continue;

        $events = $minigame->events()->whereIn('type',[
            GbEvent::TYPE_HIT,
            GbEvent::TYPE_ERROR,
        ])->whereNotNull('response_time')
            ->orderBy('datetime')->get();

        $first = $events->first();

        $minigame->first_reaction = null;
        $minigame->response_time_avg = null;
        $minigame->response_time_hits = null;
        $minigame->response_time_error = null;

        if ($first) {
            //$this->info('FIRST ' . $first->seconds);
            $minigame->first_reaction = $first->seconds;
        }
        $avg = $events->avg('response_time');

        $hitEvents = $events->filter(function($item) {
            return $item->type == GbEvent::TYPE_HIT;
        });

        $errorEvents = $events->filter(function($item) {
            return $item->type == GbEvent::TYPE_ERROR;
        });

       /* if($errorEvents) {
            $minigame->tem_tr_de_erros = 1;
        } else {
            $minigame->tem_tr_de_erros = 0;
        }
        */
        $avgHits = $hitEvents->avg('response_time');
        $avgError = $errorEvents->avg('response_time');

        $minigame->response_time_hits = $avgHits;
        $minigame->response_time_error = $avgError;

        //$avg = $events->avg('response_time');
        $minigame->response_time_avg = $avg;

        $result = DB::table('event')->select(DB::raw("STDDEV(response_time) as std"))
            ->where('match_id', $minigame->id)
            ->whereNotNull('response_time')
            ->whereIn('type',[
                GbEvent::TYPE_HIT,
                GbEvent::TYPE_ERROR,
            ])
            ->first();

        $resultHit = DB::table('event')->select(DB::raw("STDDEV(response_time) as std"))
            ->where('match_id', $minigame->id)
            ->whereNotNull('response_time')
            ->whereIn('type',[
                GbEvent::TYPE_HIT
            ])
            ->first();

        $minigame->response_desvio = $result->std;
        $minigame->response_desvio_hit = $resultHit->std;

        $minigame->save();
        //$this->info('AVG '. $avg);

        if ($minigame->response_desvio != $minigame->response_desvio_hit) {
            if( abs($minigame->response_desvio - $minigame->response_desvio_hit) <= 0.2) {
                //continue;
            }

            $qtd = $events->count();
            $qtdHits = $hitEvents->count();

            $this->info('AVG  -'. $avg);
            $this->info('AVGHI-'. $avgHits);
            $this->info('QTD  -'. $qtd);
            $this->info('DVTOT '. $minigame->response_desvio);
            $this->info('QTDHI-'. $qtdHits);
            $this->info('DVHIT '. $minigame->response_desvio_hit);
            $this->info(' ');

            if($minigame->tem_tr_de_erros==0) {
                /*            $avgError = $events->filter(function($item) {
                                return $item->type == GbEvent::TYPE_ERROR;
                            })->count();

                            $avgHit = $events->filter(function($item) {
                                return $item->type == GbEvent::TYPE_HIT;
                            })->count();*/

                $total = $events->filter(function($item) {
                    return true;
                    //return $item->type != GbEvent::TYPE_HIT;
                })->avg('response_time');

                //dd('exlcui', $avgError, $avgHits, $total);
                dd('exlcui', $events->toArray(), $hitEvents->toArray(), $errorEvents->toArray(), $hitEvents->count());
            }

        }

        if ($avg > 5) {
            $this->info('OOOOOOOOOOOOOOOOOOOOOOOOOOOO');
        }
    }

});

Artisan::command('gb:corrige-falso-acerto', function (EventService $eventService) {

    $minigames = $eventService->minigamesAptos();

    $minigamesL3 = $minigames->where('level_id', 3)
        ->where('alvo', 'Cacador|Coruja|Uruca');

    $minigamesL7 = $minigames->where('level_id', 7)
        ->where('alvo', 'Coruja|Queixao|Uruca');


    foreach ($minigamesL3 as $minigame) {

        $events = $minigame->events()->orderBy('datetime')->get();
        foreach($events as $event) {
            $this->info("Ev $event->id");
            $eventService->atualizaFalsoAcerto("Cacador", $event);
        }
    }
    $this->info('total  L3 = ' . $minigamesL3->count());
    sleep(1);

    foreach ($minigamesL7 as $minigame) {

        $events = $minigame->events()->orderBy('datetime')->get();
        foreach ($events as $event) {
            $this->info("Ev $event->id");
           // $eventService->atualizaFalsoAcerto("Uruca", $event);
        }
    }
    $this->info('total  L7 = ' . $minigamesL7->count());

});

Artisan::command('gb:senso', function (EventService $eventService) {
    $this->info('verificando qtd eventos em geral');
    $minigames = $eventService->minigamesAptos();

   // $minigames = $minigames->where('level_id', 3)
        //->where('alvo', 'Cacador|Coruja|Uruca');
      ;//  ->where('alvo', 'Coruja|Queixao|Uruca');

    //dd($minigames->count());
    $this->info('total = ' . $minigames->count());
    foreach ($minigames as $minigame) {

        $events = $minigame->events()->orderBy('datetime')->get();

        if($minigame->tem_objetos ==  0 ) {

        } else {
              $eventService->atualizaOmissoesComObjeto($events);
//            $eventService->atualizaTempoRespostaComObjeto($events);

            /*$comTr = $comTr->map(function ($item) {
                return $item->map(function ($j){
                    return [
                        'nome' => $j->name,
                        'alvo' => $j->click_target,
                        'tipo' => $j->type
                        ];
                });
            });*/
        }

        $hits = $events->map(function ($item) {
            $types = [
                GbEvent::TYPE_GAME_SHOWED_TARGET,
                GbEvent::TYPE_ERROR,
                GbEvent::TYPE_HIT,
                GbEvent::TYPE_GAME_OMISSION
            ];

            if(in_array($item->type, $types)) {

                if($item->target_size == null) {
                    $this->info("item $item->id");
                    $item->target_size = $item->getTargetSize();
                    $item->save();
                }
            }
        });

        $hits = $events->filter(function ($item) {
            return $item->type == GbEvent::TYPE_HIT;
        });

        $errors = $events->filter(function ($item){
            return $item->type == GbEvent::TYPE_ERROR;
        });
        $aleatory = $events->filter(function ($item){
            return $item->type == GbEvent::TYPE_ALEATORY_ACTION;
        });

        $omissoes = $events->filter(function ($item) {
            return $item->type == GbEvent::TYPE_GAME_OMISSION && $item->omissao == 1;
        });


        $this->info("Om {$omissoes->count()} hits {$hits->count()} err {$errors->count()} ale {$aleatory->count()}");

        if ($minigame->omissions != $omissoes->count() ) {
            //dd($minigame->toArray());
        }
    }
    $this->info('OK');
});
Artisan::command('gb:atualiza-lista-alvos', function (EventService $eventService) {
    $this->info("Atualizando qtd eventos");

    $minigames = $eventService->minigamesAptos();

    foreach ($minigames as $minigame) {
        $alvos = [];
        $naoalvos = [];
        $events = $minigame->events()->get();

        $events = $events->filter(function($item) {
            $eventosAnalisados = [
                GbEvent::TYPE_GAME_SHOWED_TARGET,
                GbEvent::TYPE_HIT,
                GbEvent::TYPE_ERROR,
                //GbEvent::TYPE_GAME_OMISSION
            ];

            return in_array($item->type, $eventosAnalisados);
        });

        foreach($events as $event) {
            $objeto = $event->getTargetName();
            if($event->type == GbEvent::TYPE_GAME_SHOWED_TARGET) {

                if($event->name == 'Alvo apareceu') {
                    if (!in_array($objeto, $alvos)) {
                        $alvos[] = $objeto;
                    }
                }

                if($event->name == "Nao Alvo apareceu") {
                    if (!in_array($objeto, $naoalvos)) {
                        $naoalvos[] = $objeto;
                    }
                }

                //dd(''$event->toArray());
            }

            if($event->type == GbEvent::TYPE_HIT) {
                if (!in_array($objeto, $alvos)) {
                    $alvos[] = $objeto;
                }
            }

            if($event->type == GbEvent::TYPE_ERROR) {
                if (!in_array($objeto, $naoalvos)) {
                    $naoalvos[] = $objeto;
                }
            }
        }
        sort($alvos);
        sort($naoalvos);
        //if($minigame->level_id == 3) {
            $minigame->nao_alvos = implode('|',$naoalvos);
            $minigame->save();

            $alvosSalvo = explode('|', $minigame->alvo);
            sort($alvosSalvo);

            $alvosSalvo = implode('|', $alvosSalvo);

            $intersect = array_intersect($alvos, $naoalvos);
            if(!empty($intersect)){
                $minigame->alvo_e_nao_alvo = 1;
                $minigame->save();
            }

            $alvos = implode('|', $alvos);
            if ($alvosSalvo !=  $minigame->alvo) {
                $minigame->alvo = $alvos;
                $minigame->save();

                $this->info($alvosSalvo . ' -'. $alvos);
        //        dd($minigame->alvo, $alvos);
            }
        //$this->info($alvosSalvo, $alvos);
            //$this->info('> ' .$minigame->nao_alvos);
        //}
    }
});

// contabiliza qtd eventos do matchjson em match ps atualizado extract para pegar essa info
Artisan::command('gb:atualiza-qtd-eventos', function (EventService $eventService) {
    $this->info("Atualizando qtd eventos");

    $minigames = $eventService->minigamesAptos();

    foreach ($minigames as $minigame) {
        $this->info("Atualizando mg $minigame->id");
        $matches = MatchJson::where('sessionId', $minigame->game_session_id)->orderBy(DB::raw('LENGTH(payload)'), 'desc')->get();

        if ($matches->count() > 1) {
            $a = strlen($matches[0]->payload);
            $b = strlen($matches[1]->payload);

            //dd($matches->toArray());
            /// $a != $b ||
            if ($matches->count() > 2) {
                throw new Exception("duplicidade - {$matches->count()} {$a} {$b}");
            }
        }
        $match = $matches->first();

        $json = json_decode($match->payload);
        $totalJsonEvents = count($json->gameEvents);

        $minigame->total_eventos = $totalJsonEvents;
        $minigame->save();
    }

    $this->info('OK');
});


Artisan::command('gb:checa-qtd-eventos', function (EventService $eventService) {
    $this->info('verificando qtd eventos ');
    $minigames = $eventService->minigamesAptos();

    foreach ($minigames as $minigame) {

        $totalMatchEvents = $minigame->total_eventos;
        $totalEvents = $minigame->events()->orderBy('datetime')->count();

        if ($totalEvents != $totalMatchEvents) {
            $this->info("Qtd eventos diferente: match $minigame->id -  EV: $totalEvents <> JSON: $totalMatchEvents");
            $this->call('gb:confere-matchjson');
            $this->call('gb:atualiza-qtd-eventos');
        }
    }
    $this->info('OK');
});

//calcula qual nivel maximo jogado no MINIGAME 6
Artisan::command('gb:nivel-maximo', function (EventService $eventService) {
    $this->info('iniciando');

    // valores default para inicio da contagem
    DB::table('player')
        ->whereNull('nivel_mais_alto')
        ->update(['nivel_mais_alto' => 0]);

    $minigame = 6;
    $nivel = 1;

   /* DB::table('match')->select('player_id')
        ->where('minigame_id', $minigame)
        ->where('level_id', $nivel)
        ->pluck('id');*/

    //$count = 'select * from `match` where player_id = 139 and minigame_id = 6'

    //jogou nivel 0
    $ids = DB::table('mi6ni0')->select('id')->pluck('id');
    DB::table('player')->whereIn('id', $ids)->update(['jogou_nivel_zero' => 1]);

    for($nivel = 1; $nivel <= 10; $nivel++) {
        $tabela = 'mi6ni'. $nivel;
        $this->info($tabela);

        $ids = DB::table($tabela)->select('id')->pluck('id');
        //dd($ids->toArray());

        DB::table('player')
            ->where('nivel_mais_alto', $nivel - 1)
            ->whereIn('id', $ids)
            ->update(['nivel_mais_alto' => $nivel]);
    }

    $this->info('OK');
});

Artisan::command('gb:atualiza-partidas-aptas', function (EventService $eventService) {
    $this->info('atualiza partidas aptas');
    $eventService->atualizaPartidasAptas();
    $this->info('OK');
});

Artisan::command('gb:testa-range', function (EventService $eventService) {
    $this->info('testa range');
    $eventService->testRange();
    $this->info('OK');
});

/**
 * @deprecated ja esta em fix-seconds
 */
Artisan::command('gb:ajusta-segundos-inteiro', function (EventService $eventService) {
    $this->info('ajusta segundo');
    $eventService->ajustaSegundosInteiro();
    $this->info('OK');
});

Artisan::command('gb:verifica-contagens', function (EventService $eventService, GamebookService $gbService) {
    $this->info('Verifica se as contagens batem com o n de eventos');

    $minigames = $eventService->minigamesAptos();
    foreach ($minigames as $minigame) {
        $erros = $minigame->events()->where('type', GbEvent::TYPE_ERROR)->count();
        $acertos = $minigame->events()->where('type', GbEvent::TYPE_HIT)->count();
        $omissoes = $minigame->events()->where('type', GbEvent::TYPE_GAME_OMISSION)->count();
        $aleatorios = $minigame->events()->where('type', GbEvent::TYPE_ALEATORY_ACTION)->count();
        $aparecimentos = $minigame->events()->where('type', GbEvent::TYPE_GAME_SHOWED_TARGET)->count();

        if ($minigame->hits == $acertos) {
            //$this->info('hits ok');
        }
        else {
            $this->info("ERROS HITS {$minigame->hits} {$acertos}");
        }

        if ($minigame->errors == $erros) {
            //$this->info('error ok');

        } else {
            $this->info("ERROS!  {$minigame->errors} {$erros}");
        }

        if ($minigame->aleatory_actions == $aleatorios) {
            //$this->info('aleat ok');
        } else {
            $this->info("$minigame->id ERROS ALEAT {$minigame->aleatory_actions} {$aleatorios}");
        }

        if($aparecimentos == ($omissoes)) {
            $this->info($minigame->datetime.' Aparece e some ok');
        } else {
            if($aparecimentos == $omissoes) {
                $this->info("##omissoes - $acertos + $erros + $omissoes");
            }

            if($aparecimentos == $acertos + ($omissoes - $acertos)) {
                $this->info("##Acertos + (omissoes -  acertos)");
            }

            if($aparecimentos == $acertos + $erros) {
                $this->info("##Acertos + erros");
            }

            if($aparecimentos == $acertos + $erros  ) {
                //$this->info("Acertos + erros");
            }
            dd($minigame->id. " ap  $aparecimentos - om $omissoes er {$erros} ac {$acertos}");
        }

        if($aparecimentos < $omissoes + ($acertos - $erros)) {
            $this->info('Nao conta erros ok');
        }
        if($aparecimentos < $omissoes) {
            $this->info('Nao conta erros ok');
        }
        //calc de omissao é($acertos + $erros) - $omissoess;
        if ($minigame->omissions == ($acertos + $erros) - $omissoes) {
        //if ($minigame->omissions == ($acertos + $erros)) {
        //if ($minigame->omissions == $omissoes) {
            //$this->info('OMISSOES ok');
        } else {
            //$minigame->omissions = ($acertos + $erros) - $omissoes;
            //$minigame->save();
            $this->info("ERROS OMISSOES {$minigame->omissions} {$omissoes}");
        }
    }
});

Artisan::command('gb:confere-matchjson', function (EventService $eventService, GamebookService $gbService) {
    $this->info('Verifica se eventos correspondem em match_json');

    $minigames = $eventService->minigamesAptos();
    foreach ($minigames as $minigame) {
        $matches = MatchJson::where('sessionId',$minigame->game_session_id)->orderBy(DB::raw('LENGTH(payload)'), 'desc')->get();

        if($matches->count()> 1) {
             $a = strlen($matches[0]->payload);
             $b = strlen($matches[1]->payload);

            //dd($matches->toArray());
            /// $a != $b ||
            if($matches->count() > 2) {
                throw new Exception("duplicidade - {$matches->count()} {$a} {$b}");
            }
        }
        $match = $matches->first();


        $json = json_decode($match->payload);
        $json->gameEvents = collect($json->gameEvents)->sortBy('datetime');
        $totalJsonEvents  = count($json->gameEvents);
        $events = $minigame->events()->orderBy('datetime')->get();
        $totalEvents = $events->count();


        if($totalEvents != $totalJsonEvents) {

            $this->info("totais diferentes - match {$minigame->id} -- json {$totalJsonEvents} - guardado {$totalEvents}");

            for( $k = 0; $k < $totalJsonEvents; $k++) {

                // ajusta formato
                if($json->gameEvents[$k]->targetX) {
                    if(false === strpos( $json->gameEvents[$k]->targetX, '.')) {
                        $json->gameEvents[$k]->targetX .= '.0';
                    }
                }
                if($json->gameEvents[$k]->targetY) {
                    if(false === strpos($json->gameEvents[$k]->targetY, '.')) {
                        //dd(str_contains('.', $json->gameEvents[$k]->targetY),$json->gameEvents[$k]->targetY);
                        $json->gameEvents[$k]->targetY .= '.0';
                    }
                }

                $exist = $events->where('match_id', $minigame->id)
                    ->where('name', $json->gameEvents[$k]->eventName)
                    ->where('click_target', $json->gameEvents[$k]->targetClick)
                    ->where('target_x', $json->gameEvents[$k]->targetX)
                    ->where('target_y', $json->gameEvents[$k]->targetY)
                    ->where('datetime', str_pad($json->gameEvents[$k]->dateTime, 25, "0"))
                    ->where('object_id',$json->gameEvents[$k]->objectId)
                    ->count();

                if($exist === 0) {
                    /*if($match->id == 4581) {
                        $existeSemObjId =  $events->where('match_id', $minigame->id)
                            ->where('name', $json->gameEvents[$k]->eventName)
                            ->where('click_target', $json->gameEvents[$k]->targetClick)
                            ->where('target_x', $json->gameEvents[$k]->targetX)
                            ->where('target_y', $json->gameEvents[$k]->targetY)
                            ->where('datetime', str_pad($json->gameEvents[$k]->dateTime, 25, "0"));

                        dd($existeSemObjId);
                    }*/

                    //dd(str_pad($json->gameEvents[$k]->dateTime, 25 , "0"), $events[0]->datetime);

                    if (is_null($json->gameEvents[$k]->objectId) || $json->gameEvents[$k]->objectId == 0 ) {
                        $existeSemObjId =  $events->where('match_id', $minigame->id)
                            ->where('name', $json->gameEvents[$k]->eventName)
                            ->where('click_target', $json->gameEvents[$k]->targetClick)
                            ->where('target_x', $json->gameEvents[$k]->targetX)
                            ->where('target_y', $json->gameEvents[$k]->targetY)
                            ->where('datetime', str_pad($json->gameEvents[$k]->dateTime, 25, "0"));

                        if($existeSemObjId->count() > 1 ) {
                            dd('Mais de 1 com parecido');
                        }
                       if($existeSemObjId->first()->object_id != $json->gameEvents[$k]->objectId) {
                           dd('haha', $existeSemObjId->first()->object_id , $json->gameEvents[$k]->objectId);
                       }
                    }
/*
                   $R =  $events->where('match_id', $minigame->id)
                        ->where('name', $json->gameEvents[$k]->eventName)
                        ->where('click_target', $json->gameEvents[$k]->targetClick)
                        ->where('target_x', $json->gameEvents[$k]->targetX)
                        ->where('target_y', $json->gameEvents[$k]->targetY)
                        ->where('datetime', str_pad($json->gameEvents[$k]->dateTime, 25, "0"))
                        ;*/

                        //dd($R->toArray(), $json->gameEvents[$k]);

                    $this->info('SALVANDO '. $json->gameEvents[$k]->eventName .' '.$json->gameEvents[$k]->targetX);
                    $gbService->newEvent($minigame->id, $json->gameEvents[$k]);
                } else {
                   // dd($exist);
                }

                continue;

                if ($json->gameEvents[$k]->targetClick != $events[$k]->click_target) {

                    $this->info('ta',$k,$events[$k]->toArray(), $json->gameEvents[$k]);
                }
            }
            continue;
dd('fim');

//            dd('naoiguais',$match->toArray(), $json->gameEvents[$k]);

            // se tiver sobrando no log
            if($totalJsonEvents < $totalEvents) {
                /*for( $k = 0; $k < $totalEvents; $k++) {
                    if($json->gameEvents[$k]->targetX != $events[$k]->target_x){
                        dd('ta',$events[$k]->toArray(), $json->gameEvents[$k]);
                    }
                }*/


                  //  dd($match->toArray(), $json->gameEvents[$k]);
                throw new Exception('eventos sobrando no log');
            }

            $contador= 0;
    //        do {

                $events = $minigame->events()->orderBy('datetime')->get();
                    try {
                        //exit;
                        //dd($events);
                        $countEvents  = count($events);
                        $this->info(" $countEvents eventos");
                        for( $k = 0; $k < $totalJsonEvents; $k++) {
                            $this->info(" k $k - {$totalJsonEvents} {$countEvents}");

                            if(!isset($json->gameEvents[$k]->objectId)){
                                $json->gameEvents[$k]->objectId = null;
                            }


                            $exist = $events->where('match_id', $minigame->id)
                                ->where('name', $json->gameEvents[$k]->eventName)
                                ->where('click_target', $json->gameEvents[$k]->targetClick)
                                ->where('target_x', $json->gameEvents[$k]->targetX)
                                ->where('target_y', $json->gameEvents[$k]->targetY)
                                ->where('datetime',$json->gameEvents[$k]->dateTime .'000')
                                //->where('object_id',$json->gameEvents[$k]->objectId)
                                ->exists();


                            if ($exist) {
                                $this->info('ja existe '. $json->gameEvents[$k]->eventName);
                                if($minigame->id == 4728) {
                                    foreach($events as $e) {
                                        if ($e->object_id == $json->gameEvents[$k]->objectId) {
                                            $this->info('achei'. $e->id);
                                        }
                                    }
                                    //dd($exist, $json->gameEvents[$k], $events[$k]->toArray());
                                }
                                continue;
                            } else {
                                $this->info('salvae '. $json->gameEvents[$k]->eventName .' '.$json->gameEvents[$k]->targetX);
                                $gbService->newEvent($minigame->id, $json->gameEvents[$k]);
                                continue;
                            }


                            $event = $events[$k];
                            $this->info("verificando evento {$event->id}");
                            $this->info($event->id.' '.substr($event->datetime, 0,23) .' :: '. $json->gameEvents[$k]->dateTime);


                            $dataDiferente = substr($event->datetime, 0,23) != $json->gameEvents[$k]->dateTime;
                            $nomeDiferente = $event->name != $json->gameEvents[$k]->eventName;
                            $alvoDiferente = $event->click_target != $json->gameEvents[$k]->targetClick;
                            $objetoDiferente = $event->object_id != $json->gameEvents[$k]->objectId;

                            if ($dataDiferente || $nomeDiferente || $alvoDiferente || $objetoDiferente) {
            //                    dd([$k,$event->toArray(), $json->gameEvents[$k]], $dataDiferente , $nomeDiferente , $alvoDiferente, $objetoDiferente);
                                $exist = GbEvent::where('match_id', $event->match_id)
                                    ->where('name', $json->gameEvents[$k]->eventName)
                                    ->where('click_target', $json->gameEvents[$k]->targetClick)
                                    ->where('datetime',$json->gameEvents[$k]->dateTime .'000')
                                    ->where('object_id',$json->gameEvents[$k]->objectId)
                                    ->exists();
                                $this->info('ja existe '. $json->gameEvents[$k]->eventName);
                                if($exist) continue;

                                $gbService->newEvent($event->match_id, $json->gameEvents[$k]);
                                break;
                                //dd([$event->toArray(), $json->gameEvents[$k]]);
                                // recomeca
                                //$k--;


                            }
                       }
                        //$events = $minigame->events()->orderBy('datetime')->get();

                        $contador = 0;
                    }   catch (Exception $e){
                        $this->info('Erro '.$e->getMessage());
                        $contador++;
                        $this->info($contador);
                    }

  //          } while(count($events) != $totalJsonEvents);
            //throw new Exception('sucesso');
        }

    }
    $this->info('okay');
});

Artisan::command('gb:tem-objeto', function (EventService $eventService) {
    $this->info('Calculando tempo de resposta');

    $minigames = $eventService->minigamesAptos();
    echo 'minigames '. $minigames->count() . PHP_EOL;


        $minigamesSemObjeto = $minigames->filter(function ($item) {
            return $item->tem_objetos == 0;
        });

        foreach ($minigames as $m)  {
            $qtd = $m->events()//->whereNotNull(' object_id')
                //->where('object_id', "!=", '0')
                    ->where('type', GbEvent::TYPE_GAME_SHOWED_TARGET)
                    ->where('name','<>', 'Alvo apareceu')
                ->count();

            if($qtd == 0 ) {
                if ($m->alvo_exclui_erros == 0) {
                    $m->alvo_exclui_erros = 1;
                    $m->save();
                }
                $this->info("sem eventos de show_target de erros {$m->id} {$m->alvo_exclui_erros}");
            } else {
                if ($m->alvo_exclui_erros == 1) {
                    dd('ta errado issai',$m->id);
                }
                if($m->tem_objetos == 0) {
                    dd('nao tem objeto no show erroor', $m->id);
                }
            }
        }
    $this->info('minigames ');
});

/*
 * EVENTOS:
 * Game 6 ignorando meses anteriores a maio
 */
Artisan::command('gb:tempo-resposta', function (EventService $eventService) {
    $this->info('Calculando tempo de resposta');

    $minigames = $eventService->minigamesAptos();
    echo 'minigames '. $minigames->count() . PHP_EOL;

    try {
        $minigamesSemObjeto = $minigames->filter(function($item) {
           return $item->tem_objetos == 0;
        });


        $minigamesComObjeto = $minigames->filter(function($item){
            return $item->tem_objetos == 1;
        });

        $this->info('Atualizando registros Com object_id');
        //$eventService->atualizaRegistrosSemObjetctId($minigamesSemObjeto);
        //$eventService->atualizaRegistrosSemObjetctId($minigamesComObjeto);
        $this->info('Atualiza tempo de resposta');
        //sleep(3);
        $eventService->atualizaTempoResposta($minigamesComObjeto);
    } catch (\Exception $e) {
        echo $e->getTraceAsString();
        echo $e->getMessage();
    }
});

Artisan::command('gb:calcula-omissoes', function (EventService $eventService) {
    $this->info('Calculando omissao');

    $minigames = $eventService->minigamesAptos();
    echo 'minigames '. $minigames->count() . PHP_EOL;

    try {
        $minigamesSemObjeto = $minigames->filter(function($item) {
            return $item->tem_objetos == 0;
        });


        $minigamesComObjeto = $minigames->filter(function($item){
            return $item->tem_objetos == 1;
        });

        $this->info('Atualizando registros Com object_id');
        //$eventService->atualizaRegistrosSemObjetctId($minigamesSemObjeto);
        //$eventService->atualizaRegistrosSemObjetctId($minigamesComObjeto);
        $this->info('Atualiza tempo de resposta');
        //sleep(3);
        //echo count($minigames).' minig' . PHP_EOL;
        foreach($minigames as $m) {
            $events = GbEvent::where('match_id', $m->id)->orderBy('datetime')->get();
            echo "Partida $m->id - ".count($events).' Eventos, jogador '.$m->player->player_name . PHP_EOL;
            $eventService->atualizaOmissao($events);
            //break;
        }


    } catch (\Exception $e) {
        echo $e->getTraceAsString();
        echo $e->getMessage();
    }
});


Artisan::command('gb:fix-players', function (ExtractService $extractService) {
    $this->info('Retirando players');

    // jogadores com nome+idade igual
    $imports = Player::selectRaw('id, player_name, age,count(player_name) as counted')
        ->groupBy('player_name')
        ->groupBy('age')
        ->havingRaw('counted > 1')
        ->get();

    $imports->map(function($item) {

        $duplicateds = Player::selectRaw('id, player_name, age')
            ->where('player_name', $item->player_name)
            ->where('age', $item->age)
            ->where('id', '<>', $item->id)
            ->get();
        //dd($duplicateds->toArray());

        foreach ($duplicateds as $k =>$item) {
            $this->info($item->player_name);
            $item->excluir = 1;
            $item->save();
        }
    });


    $this->info($imports->count().' Registros afetados');
});

Artisan::command('gb:reset', function () {
    $this->info('Resetando ... o.O');

    $this->call('gb:clear');
    Import::where('extracted','<>', '-1')->update(['extracted' => 0, 'qtd_matches' => 0, 'comments' => '']);
    MatchJson::query()->truncate();

    $this->call('gb:fix-imports');
    $this->info('OK');
});

Artisan::command('gb:clear', function () {
  $this->info('limpando tabelas auxiliares');
  MatchJson::where('extracted','<>', '-1')->update(['extracted' => 0, 'obs' => '','status' => 0]);
  GbMatch::query()->truncate();
  GbEvent::query()->truncate();

  $this->info('Tudo Limpo');
});


Artisan::command('gb:fix-imports', function (ExtractService $extractService) {
    $this->info('Retirando imports duplicados');
    $total = $extractService->fixDuplicatedImports();
    $this->info($total.' Registros afetados');
});

Artisan::command('gb:fix-matchesjson-duplicado', function (ExtractService $extractService) {
    $this->info('Retirando matches duplicados');
    $total = $extractService->fixDuplicatedMatchesJson();
    $this->info($total.' Registros afetados');
});

Artisan::command('gb:extract-match', function (ExtractService $extractService) {
  $start = date('H:i:s');
  $this->info("start " . $start);

  //$this->info("Corrigindo registros duplicados ");

  $extractService->fixDuplicatedMatchesJson();

  $matches = (new MatchJson)->toExtract()->limit(200)->get();
  do{

      $this->info("importando {$matches->count()} registros");
      foreach($matches as $k => $match) {
        $this->info("match {$match->id}");
        DB::beginTransaction();
        try {
            $extractService->extractMatchJson($match);
            DB::commit();
        } catch(\Exception $e) {

          DB::rollback();

          if($e instanceof \ErrorException){
            $match->json_status = 1;
            //$match->extracted = 1;
            // $match->save();
    //        DB::commit();
          } else{
            $match->obs =  $e->getMessage();
          }
          $match->extracted = 1;
          $match->save();

          if($k % 200 == 0) {
            sleep(2);
          }

          $this->info('ERRO ==> Match '.$match->id.' ' .$e->getMessage() );
        }
      }
      $this->info('dormindo');
      sleep(3);
      $matches = (new MatchJson)->toExtract()->limit(200)->get();
    } while($matches->count() > 0);

  $this->info("END!! Started at $start - Ended at "  . date('H:i:s'));
});

Artisan::command('gb:extract', function (ExtractService $extractService) {
  $start = date('H:i:s');
  $this->info("start " . $start);

  $errors = [];
  //$gamebookService = new GamebookService();

  $imports = (new Import())->toExtract()->get();

  if (!$imports) {
      $this->info("Nada para importar");
  }

  do{
      $this->info("importando {$imports->count()} registros");
      foreach ($imports as $k=> $importModel) {

          //DB::beginTransaction();
          $this->info("extraindo $importModel->id - ". date('H:i:s') );
          try {
              $extractService->extractImport($importModel);
              //DB::commit();
          } catch (\Exception $e) {
              $this->info("ERRO {$e->getMessage()}". $e->getTraceAsString());
              $errors[] = $e->getMessage();
              //DB::rollback();
              $importModel->comments .= $e->getMessage();
              $importModel->extracted = Import::JSON_INVALIDO;
              $importModel->save();
              throw $e;
          }
      }

      $imports = (new Import())->toExtract()->get();

    } while($imports->count() > 0);

  $this->info("END!! Started at $start - Ended at "  . date('H:i:s'));

})->describe('extract');


Artisan::command('gb:fix-seconds', function (ExtractService $extractService, EventService $eventService) {

    $this->info("Atualiza segundos");
    $matches = $eventService->minigamesAptos();

    foreach($matches as $match) {
        $this->info("Match $match->id");
        $extractService->updateEventSeconds($match);
    }

    $this->info("END");
    return;
});

Artisan::command('gb:bkp', function (ExtractService $extractService) {
    $start = date('H:i:s');
    $this->info("start " . $start);

    $players = \App\Models\GbPlayer::get();
    $i = 0;
    $limit = 100;
    do{
        $matches = GbMatch:://where('status', '<>', MatchJson::STATUS_DUPLICADO)
           //->
            with('events')
            ->limit($limit)
            ->offset($i * $limit)
            ->orderBy('id')
            ->get();

        $this->info(''. $matches->count());

        foreach ($matches as $match) {
            $event = $match->events->last();

            if(GbEvent::TYPE_GAME_FINISH == $event->type) {
                $match->match_result = 'GANHOU';
            }

            if(GbEvent::TYPE_GAME_OVER == $event->type) {
                $match->match_result = 'PERDEU';
            }

            $match->save();
            if(!$match->match_result){
                $match->match_result ='SR';
                dd($match);
            }

            $this->info($match->match_result);

        }
        $i++;
        $this->info("page " . $i .' off'. ($i * $limit));
    } while($matches->count()> 0);


    $this->info("END!! Started at $start - Ended at "  . date('H:i:s'));

});



Artisan::command('gb:fix-owners', function (ExtractService $extractService) {
    $start = date('H:i:s');
    $this->info("start " . $start);

    $dups = \App\Models\GbPlayer::where('id_owner','<>', null)->get();

    $dups->map(function($item) {
        $matches = GbMatch::where('player_id', $item->id)->get();
        if($matches->count()== 1) {
            return ;
        }

        foreach($matches as $match) {
            if(is_null($item->id_owner)) {
                throw new \Exception('ID onwer nao pode ser nulo');
            }

            $this->info('partida ' . $match->id);
            $this->info('jogador original ' . $match->player_id.'-'.$item->player_name);
            $this->info('jogador novo ' . $item->id_owner);
            $match->player_id = $item->id_owner;
            $match->save();
        }

    });
    $this->info('total '.$dups->count());
    $this->info("END!! Started at $start - Ended at "  . date('H:i:s'));

});

Artisan::command('gb:order-matches', function (ExtractService $extractService) {
    $start = date('H:i:s');
    $this->info("start " . $start);

    $players = \App\Models\GbPlayer::get();
    $i =0;
    $limit =100;

    foreach($players as $player) {
        $this->info('jogador '. $player->player_name);
        for($mg = 1 ; $mg <= 6; $mg++) {
            $this->info("GAME " . $mg);
            for($i = 0; $i<=10; $i++) {
                $matches = GbMatch:://where('status', '<>', MatchJson::STATUS_DUPLICADO)
                where('minigame_id', $mg)->where('player_id', $player->id)->orderBy('datetime')->get();

                foreach($matches as $k=> $match) {
                    $match->match_order = ($k +1);
                    $match->save();
                }

                $this->info('nivel '. $i);
                $this->info('partidas '.$matches->count());
            }
        }

    }

    $this->info("END!! Started at $start - Ended at "  . date('H:i:s'));

});

Artisan::command('gb:order-levels', function (ExtractService $extractService) {
    $start = date('H:i:s');
    $this->info("start " . $start);

    $players = \App\Models\GbPlayer::get();
    $i =0;
    $limit =100;

    foreach($players as $player) {
        $this->info('jogador '. $player->player_name);
        for($mg = 1 ; $mg <= 6; $mg++) {
            $this->info("GAME " . $mg);
            for($i = 0; $i<=10; $i++) {
                $matches = GbMatch:://where('status', '<>', MatchJson::STATUS_DUPLICADO)
                where('minigame_id', $mg)
                    ->where('player_id', $player->id)
                    ->where('level_id', $i)
                    ->orderBy('level_id')
                    ->orderBy('datetime')
                    ->get();

                foreach($matches as $k=> $match) {
                    $match->match_level_order = ($k +1);
                    $match->save();
                }

                $this->info('nivel '. $i);
                $this->info('partidas '.$matches->count());
            }
        }

    }

    $this->info("END!! Started at $start - Ended at "  . date('H:i:s'));

});


//SELECT * FROM `event` WHERE match_id in (SELECT id FROM `match` WHERE `minigame_id` = 6)

Artisan::command('gb:views', function (ExtractService $extractService) {

    $sql =    <<<SQL
CREATE VIEW `#VIEW` AS  
select `bkp`.`player`.`entity`, count(`bkp`.`player`.`id`) as qtd from `bkp`.`player`
where (isnull(`bkp`.`player`.`excluir`) and `bkp`.`player`.`entity` not in('CV', 'TE')
and `bkp`.`player`.`id` in (select `bkp`.`match`.`player_id` from `bkp`.`match` 
  where ((`bkp`.`match`.`level_id` = #LV)  and (`bkp`.`match`.`minigame_id` = #MG))))
GROUP by(`bkp`.`player`.`entity`)

SQL;


    $sql =    <<<SQL
CREATE VIEW `#VIEW` AS  
select `bkp`.`player`.`id` AS `id`,`bkp`.`player`.`player_name` AS `player_name`,`bkp`.`player`.`age` AS `age`,`bkp`.`player`.`entity` AS `entity`,`bkp`.`player`.`excluir` AS `excluir`,`bkp`.`player`.`obs` AS `obs`,`bkp`.`player`.`created_at` AS `created_at`,`bkp`.`player`.`updated_at` AS `updated_at`,`bkp`.`player`.`full_name` AS `full_name`,`bkp`.`player`.`id_owner` AS `id_owner`,`bkp`.`player`.`gender` AS `gender` 
from `bkp`.`player`
where (isnull(`bkp`.`player`.`excluir`) and `bkp`.`player`.`entity` not in('CV', 'TE')
and `bkp`.`player`.`id` in (select `bkp`.`match`.`player_id` from `bkp`.`match` 
  where ((`bkp`.`match`.`level_id` = #LV)  and (`bkp`.`match`.`minigame_id` = #MG))))
SQL;

    $sql =    <<<SQL
CREATE VIEW `#VIEW` AS  
select count(`bkp`.`player`.`entity`) as qtd, `bkp`.`player`.`entity`
from `bkp`.`player`
where (isnull(`bkp`.`player`.`excluir`) and `bkp`.`player`.`entity` not in('CV', 'TE')
#LIST) group by(`bkp`.`player`.`entity`)
SQL;

    $MGL = <<<SQL
    and `bkp`.`player`.`id` in (select id from #VN)
SQL;

    $sqlx =    <<<SQL
    DROP VIEW `#VIEW`
SQL;

    for($mg = 1 ; $mg <= 6; $mg++) {
        for($i = 2 ; $i <= 10; $i++) {
            $q = str_replace('#MG', $mg, $sql);
            $q = str_replace('#LV', $i, $q);
            $q = str_replace('#VIEW', "qtd_mi{$mg}n_1_a_{$i}", $q);
            $sub = '';
            for($z =1; $z<= $i; $z++){
                $sub.= str_replace('#VN', "mi{$mg}ni{$z}", $MGL);
            }

            $q = str_replace('#LIST', $sub, $q);


            try{
                $r = DB::statement($q);
                $this->info($r);

            } catch(Exception $e){
                $this->info($e->getMessage());
            }
        }
    }
});

Artisan::command('gb:count-targets', function (ExtractService $extractService) {
    $start = date('H:i:s');
    $this->info("start " . $start);

    $players = \App\Models\GbPlayer::get();
    $i =0;
    $limit =100;
    do{
        $matchesj = GbMatch::with('events')->where('minigame_id', 5)->where('obs',null)//where('status', '<>', MatchJson::STATUS_DUPLICADO)
            ->limit($limit)->offset($i * $limit)->orderBy('datetime')->get();

        //$this->info($matchesj->count());

        foreach ($matchesj as $match) {
            //$json = json_decode($matchj->payload);

            //dd($json);
            //$this->info($match->datetime);

            $events = $match->events;

/*            $targets = 0;
            foreach($events as $k => $ev) {
                    if($ev->type == GbEvent::TYPE_GAME_SHOWED_TARGET) {
                        $targets++;
                    }
            }*/


//            $match->total_targets = $targets;


            $aleatory = 0;
            foreach($events as $k => $ev) {
                if($ev->type == GbEvent::TYPE_ALEATORY_ACTION) {
                    $aleatory++;
                }
            }

            if($match->aleatory_actions != $aleatory) {
                $this->info($match->id);
                if($match->aleatory_actions != 0){
                    $this->info('ALEATORY '.$aleatory .'!='.$match->aleatory_actions);
                }
                $match->aleatory_actions = $aleatory;
            }

            $errors = 0;
            foreach($events as $k => $ev) {
                if($ev->type == GbEvent::TYPE_ERROR) {
                    $errors++;
                }
            }

            if($match->errors != $errors) {
                $this->info($match->id);
                if($match->errors != 0) {
                    $this->info('Errors '.$errors .'!='.$match->errors);
                }
                $match->errors = $errors;
            }


            $hits = 0;
            foreach($events as $k => $ev) {
                if($ev->type == GbEvent::TYPE_HIT) {
                    $hits++;
                }
            }

            if($match->hits != $hits) {
                $this->info($match->id);
                if($match->hits != 0) {
                    $this->info('ACERTOS '.$hits .'!='.$match->hits);
                }
                $match->hits = $hits;
            }

            $omissions = $match->total_targets - ($hits+ $errors);
            if($omissions < 0) {
                $omissions = $omissions * -1;
            }
            $match->omissions = $omissions;
            $match->save();
        }
        $i++;
        //$this->info("page " . $i .' off'. ($i * 10));
    } while($matchesj->count()> 0);


    $this->info("END!! Started at $start - Ended at "  . date('H:i:s'));

});


Artisan::command('gb:check-owners', function (ExtractService $extractService) {
    $start = date('H:i:s');
    $this->info("start " . $start);

    $players = \App\Models\GbPlayer::get();

    do{
        $matchesj = MatchJson::where('status', '=', MatchJson::STATUS_DUPLICADO)
            ->limit(1000)->offset($i * 10)->get();

        $this->info($matchesj->count());

        foreach ($matchesj as $matchj) {
            $json = json_decode($matchj->payload);

            if ($json->playerName == "") {
                continue;
            }

            $exists = $players->contains('player_name',trim($json->playerName));

            if(!$exists) {
                $this->info('nao existe');
                $this->info("Json player " . ' - *'. $json->playerName.'*');
                dd($players->map(function($t){return $t->player_name;})->sort()->toArray());
                dd($json);

            }

            continue;

            $match = GbMatch::with('player')
                ->where('game_session_id', $matchj->sessionId)
                ->first();

            $player = Player::find($match->player_id);

            $this->info("Json player " . ' - '. $json->playerName);
            $this->info("mach player " . $match->player->id . ''.$match->player->player_name);
            $this->info("bkp player " . $player->id .' - '.$player->player_name);
        }
        $i++;
        $this->info("page " . $i .' off'. ($i * 10));
    } while($matchesj->count()> 0);


    $this->info("END!! Started at $start - Ended at "  . date('H:i:s'));

});
