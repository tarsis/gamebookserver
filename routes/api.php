<?php
use App\Models\Import;

Route::group([
    'middleware' => ['cors', 'api'],
], function () {
    Route::post('/auth/token/issue', 'AuthController@issueToken');
    Route::post('/auth/token/refresh', 'AuthController@refreshToken');
/*
    Route::group([
//        'middleware' => 'jwt.auth',
    ], function () {*/
        Route::post('/auth/token/revoke', 'AuthController@revokeToken');
        Route::get('/categories/full-list', 'CategoriesController@fullList');

        Route::resource('/categories', 'CategoriesController', [
            'except' => ['create', 'edit'],
        ]);

        Route::resource('/products', 'ProductsController', [
            'except' => ['create', 'edit'],
        ]);

        Route::get('/gamebook/test', function(){
          return Import::whereIn('extracted', [2, '-1'])->count();
        });

        Route::get('/gamebook/fix-last-match', 'GamebookReportController@fixLastMatches');
        Route::get('/gamebook/fix-duplicated-imports', 'GamebookReportController@fixDuplicatedImports');

        Route::get('/gamebook/fix-duplicated-matches', 'GamebookReportController@fixDuplicatedMatchesJson');

        Route::get('/gamebook/extract-payload', 'GamebookReportController@extractImport');
        Route::get('/gamebook/extract-payload-incomplete', 'GamebookReportController@extractImportsIncomplete');
        Route::get('/gamebook/extract-marches', 'GamebookReportController@extractMatches');


        Route::get('/gamebook/matches', 'GamebookReportController@getMatches' );
        Route::get('/gamebook/matches/{id}', 'GamebookReportController@getMatch' );
        Route::get('/gamebook/stats', 'GamebookReportController@getStats' );
        Route::get('/gamebook/events', 'GamebookReportController@getEvents' );

        Route::resource('/gamebook/fixRepeatedErrors', 'GamebookReportController@fixRepeatedErrors' );

        //Route::resource('/gamebook/metrics', 'GamebookReportController@metrics' );
        Route::resource('/gamebook/executiveFunctions', 'GamebookReportController@executiveFunctions' );
        Route::resource('/gamebook/user/{name}/gameProgress', 'GamebookReportController@gameProgress' );
        Route::resource('/gamebook/user/{name}/gameProgressDistinct', 'GamebookReportController@gameProgressDistinct' );
        Route::resource('/gamebook/user/{name}/playedByLevel', 'GamebookReportController@playedByLevel' );
        Route::resource('/gamebook', 'GamebookReportController@index');

        Route::post('/festa/cadastro', 'FestaController@cadastro');

        Route::get('/me', 'MeController@show');
    /*});*/
});
