<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

	Route::get('/login', function (Request $request)  {
	   $request->session()->put(['chave' => null]);
           return view('login');
	});
	
	Route::post('/login', function(Request $request) {
		if($request->get('passaporte') != "gamebook2018"){
			$request->session()->flash('status', 'Passaporte invalido');
			return redirect('/login');
		}

	  $request->session()->put(['chave' => 'OK']);
 	  return redirect('/listar');
	});

	Route::get('/listar', function (Request $request) {
		$value = $request->session()->get('chave');
		if('OK' != $value) {
			return redirect('/login');
		}

		$players = \App\Models\GbPlayer::where('id_owner', null)
//            ->where('entity','<>', 'CV')
            //->where('entity','=', 'RO')
            ->where('entity','<>', 'DDE')
            ->where('excluir',null)

            ->where('obs',null)
            ->orderBy('player_name')
            ->get();

	    return view('cadastro')->with('players', $players);
	});

	Route::post('/salvar', '\App\Http\Controllers\FixController@save');

