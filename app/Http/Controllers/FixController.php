<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Transformers\UserTransformer;

class FixController extends ApiController
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cadastro()
    {
        return 'cadastro';
    }


    public function entityUpdate(Request $request)
    {
        $errors = [];
        $players = \App\Models\GbPlayer::get();
        foreach ($players as $player){
            $bkp = \App\Models\Player::where('player_name', $player->player_name)
                ->where('age',$player->age)->first();
            if(!$bkp){
                $errors[] = $player->player_name;
                continue;
            }

            if($bkp->player_name != $player->player_name){
                return response()->json(['success' => false,'msg' => 'jogador invalido'
                    .$player->id.' '.$player->player_name .' - '.$bkp->id.' '.$bkp->player_name]);
            }

            $bkp->entity = $player->entity;
            $bkp->save();
        }
        return response()->json([]);

    }

    public function save(Request $request)
    {
        $this->validate($request,[
            'id' => 'required',
            'player_name' => 'required',
            'age' => 'required',
            'entity' => 'required',
        ]);
        $player = \App\Models\GbPlayer::find($request->id);

        if($player->excluir == 1){
            return response()->json(['success' => false,'msg' => 'Um dos jogadores esta marcado para exclusao']);
        }

        $player->player_name = $request->player_name;
        $player->age = $request->age;
        $player->entity = $request->entity;
        $player->full_name = $request->full_name;
        $player->obs = $request->obs;
        $player->save();

        if($request->selected) {
                $dups = \App\Models\GbPlayer::where('id','<>', $player->id)->whereIn('id', $request->selected)->orWhereIn('id_owner', $request->selected)->get();

                $dups->map(function($item) use ($player) {
                    $item->id_owner = $player->id;
                    $item->save();
                });
        }

        return response()->json(['success' => false,'msg' => 'Salvo com sucesso']);
    }

    public function donosDosDuplicados(Request $request)
    {
        $this->validate($request,[
            'id' => 'required',
            'player_name' => 'required',
            'age' => 'required',
            'entity' => 'required',
        ]);
        $player = \App\Models\GbPlayer::find($request->id);

        if($player->excluir == 1){
            return response()->json(['success' => false,'msg' => 'jogador marcado para exclusao']);
        }

        $playerDups = \App\Models\GbPlayer::where('player_name', 'like', 'DUP %')->groupBy('player_name')->get();

        foreach($playerDups as $p) {
            $name = str_replace('DUP ','', $p->player_name);

            $owner = \App\Models\GbPlayer::where('player_name',  $name)
                ->first();

            $dups = \App\Models\GbPlayer::where('player_name', 'DUP '. $owner->player_name)->get();

            $dups->map(function($item) use ($owner) {
                $item->id_owner = $owner->id;
                $item->save();
            });
            //  dd($name);
        }

        //$playerDups = \App\Models\GbPlayer::where('player_name', 'DUP '. $player->player_name)->get();

        //dd($playerDups->toArray());

        $players = \App\Models\GbPlayer::orderBy('player_name')->get();

    }
}
