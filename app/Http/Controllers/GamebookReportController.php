<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use App\MatchJson;
use App\Models\Gamebook;
use App\Models\GamebookMetrics;
use App\Models\GbEvent;
use App\Models\GbMatch;
use App\Models\GbPlayer;
use App\Models\Import;
use App\Models\ViewEvento;
use App\Models\ViewPartida;
use App\Services\GamebookService;
use App\Services\GamebookStatsService;
use App\Services\ExtractService;
use App\Transformers\CategoryTransformer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class GamebookReportController extends ApiController
{
    public function getStats()
    {
        $minigame = Input::get('minigame');
        $nivel = Input::get('nivel');

        $selectFields = ['minigame'];
        if (!empty($nivel)) {
            $selectFields[] = 'nivel';
        }
        $partidasView = ViewPartida::where('criado_em', '!=', '0001-01-01 00:00:00')
		->selectRaw(implode(',', $selectFields) . ', avg(acertos) as acertos,
                             avg(erros) as erros,
                             avg(erros_repetidos) as erros_repetidos,
                             avg(omissoes) as omissoes,
                             avg(acoes_aleatorias) as acoes_aleatorias,
                             avg(duracao) as duracao
                             ');
        if (!empty($minigame)) {
            $partidasView->where('minigame', $minigame);
        }
        if (!empty($nivel)) {
            $partidasView->where('nivel', $nivel);
        }

        $sumary = $partidasView->first();
        $sumary->duracao = (int) $sumary->duracao .' s';
        return $this->response->json($sumary->toArray());
    }

    public function getMatches()
    {
        $minigame = Input::get('minigame');
        $nivel = Input::get('nivel');
        $selectFields = ['minigame'];
        if (!empty($nivel)) {
            $selectFields[] = 'nivel';
        }
        try {
            $sumary = ViewPartida::groupBy($selectFields)
                ->selectRaw(implode(',', $selectFields) . ', avg(acertos) as acertos,
                             avg(erros) as erros,
                             avg(erros_repetidos) as errors_repetidos,
                             avg(omissoes) as omissoes,
                             avg(acoes_aleatorias) as acoes_aleatorias,
                             avg(duracao) as duracao
                             ')
                ->get();

            $partidasView = ViewPartida::where('criado_em', '!=', '0001-01-01 00:00:00')
                ->orderBy('criado_em');

            if (!empty($minigame)) {
                $partidasView->where('minigame', $minigame);
            }
            if (!empty($nivel)) {
                $partidasView->where('nivel', $nivel);
            }
            $partidas = $partidasView->get();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return $this->response->json(['data' => $partidas->toArray(), 'sumary' => $sumary->toArray()]);
    }

    public function getMatch($id)
    {
        $partida = ViewEvento::where('partida', $id)->orderBy('criado_em')->get();
        return $this->response->json($partida);
    }

    public function getEvents()
    {
        $minigame = Input::get('minigame');
        $partidas = ViewEvento::where('minigame', $minigame)->orderBy('datetime')->get();
        return $this->response->json($partidas);
    }
    /**
    * Muda status dos payloads identicos
    */
    public function fixDuplicatedImports(ExtractService $extractService)
    {
        return $extractService->fixDuplicatedImports();
    }

    /**
     * Considera apenas a partida mais recente quando encontra duplicidade
     * @return int|void
     *
     */
    public function fixLastMatches()
    {
        $imports = DB::select('SELECT game_session_id, id, duration FROM `match` where game_session_id in (SELECT game_session_id FROM `match` group by game_session_id HAVING count(game_session_id) > 1) ORDER BY `match`.`id`  DESC');

        $valid = [];

        foreach($imports as $k => $import) {
            if(!isset($valid[$import->game_session_id])) {
                $valid[$import->game_session_id] = $import->id;
            } else {
                $invalid[] = $import->id;
            }
        }

        foreach($valid as $id) {
            $obj = GbMatch::find($id);
            $obj->obs = 'DUP 1';
            $obj->save();
        }

        foreach($invalid as $id) {
            $obj = GbMatch::find($id);
            $obj->obs = 'DUP 2';
            $obj->save();
        }

        return count($invalid);
    }
    /**
     *   Aponta payloads repetidos deixando apenas 1 ativo
     */
    public function fixDuplicatedMatchesJson(ExtractService $extractService)
    {
        return $extractService->fixDuplicatedMatchesJson();
    }

    /**
     * Extrai os dados da tabela IMPORT e separa em paylods de partidas
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function extractImport(ExtractService $extractService)
    {
        $errors = [];
        //$gamebookService = new GamebookService();

        $imports = (new Import())->toExtract()->get();

        if (!$imports) {
            return $this->response->json(['message' => 'Nada para importar']);
        }

        do{
            foreach ($imports as $importModel) {

                //DB::beginTransaction();
                try {
                    $extractService->extractImport($importModel);
                    //DB::commit();
                } catch (\Exception $e) {
                    $errors[] = $e->getMessage();
                    //DB::rollback();
                    throw $e;
                }
            }

            $imports = (new Import())->toExtract()->get();

          } while($imports);

        $response['message'] = 'Importação finalizada';

        if (!empty($errors)) {
            $response['errors'] = $errors;
        }
        return $this->response->json($response);
    }



    /**
     * Extrai os dados da tabela IMPORT que vieram cortados e separa em paylods de partidas
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function extractImportsIncomplete()
    {
        $errors = [];
        $gamebookService = new GamebookService();

        $imports = (new Import())->incomplete()->get();

        if (!$imports) {
            return $this->response->json(['message' => 'Nada para importar']);
        }

        foreach ($imports as $importModel) {

            //DB::beginTransaction();
            try {
                $matches = $this->transformStringToJson($importModel->payload,true);

                //$matches = $this->transform($importModel->payload);
                if (!$matches) {
                    //$errors[] = ['error' => 'Formato inválido' . $importModel->id, 'obj' => $importModel->payload];
                    $importModel->extracted = 7;
                    $importModel->comments = 'Formato invalido';
                    $importModel->save();
                    //  DB::rollback();
                    continue;
                }

                /*                if($matches == -1) {
                                    $importModel->extracted = 3;
                                    $importModel->comments = ' VEIO CORTADO';
                                    $importModel->save();

                                    //continue;
                                    $matches = $this->transformStringToJson($importModel->payload, true);
                                }*/

                $importModel->qtd_matches = count($matches);

                foreach ($matches as $k => $match) {
                    $playerId = $gamebookService->playerExists(trim($match->playerName)."_".$match->age);

                    if (is_null($playerId)) {
                        $playerId = $gamebookService->newPlayer($match);

                        if ($playerId == null) {
                            throw new \Exception('Id do Jogador não informado!' . $k);
                        }
                    }

                    $matchExists = $gamebookService->getMatchById($match->gameSessionId);

                    try{
                        $matchJson = new MatchJson();
                        $matchJson->sessionId = $match->gameSessionId;
                        $matchJson->datetime  = $match->datetime;
                        $matchJson->payload = json_encode($match);
                        $matchJson->obs = $importModel->id;
                        $matchJson->status = $matchExists ? 1: 2;

                        $matchJson->saveOrFail();

                    } catch (\Exception $e) {
                        if($e->getCode() == 23000) {
                            //$importModel->extracted = -2;
                            $importModel->comments .= " duplicado";
                            //  throw $e;
                            //$importModel->save();
                        } else{
                            throw $e;
                        }

                        //continue 2;
                    }

                    if ($matchExists) {
                        $comment ='';
                        if($matchExists->errors != $match->totalErrors) {
                            $comment = 'ERRO ' . $matchExists->errors .' != ' . $match->totalErrors;
                            $errors[] = ['error' => 'ERRO ' . $matchExists->errors .' != ' . $match->totalErrors , 'obj' => $importModel->id, 'sid1' => $matchExists->gameSessionId, 'sid2' => $match->gameSessionId];
                        }
                        if($matchExists->hits != $match->totalHits) {
                            $comment .= 'ACERTO ' . $matchExists->hits .' != ' . $match->totalHits;
                            $errors[] = ['error' => 'ACERTO ' . $matchExists->hits .' != ' . $match->totalHits , 'obj' => $importModel->id, 'sid1' => $matchExists->gameSessionId, 'sid2' => $match->gameSessionId];
                        }
                        if($comment != '') {
                            $importModel->comments .= $comment;

                            //$importModel->extracted = 4;
                            //$importModel->save();
                        }

                        //$matchExists->errors = $match->totalErrors;
                        //$matchExists->hits = $match->totalHits;

//                        $matchExists->save();

                        //dd($matchExists->errors);
                        //dd($match->totalErrors);

                        //continue; //evita que partidas sejam salvas 2 vezes em imports q falharam
                    }


                    $matchId = $gamebookService->newMatch($playerId, $importModel->id, $match);

                    if ($match->gameEvents) {
                        foreach ($match->gameEvents as $event) {
                            /*if (!isset($event->targetClick)) {
                                $event->targetClick = '';
                            }*/
                            if (!isset($event->category)) {
                                $event->category = 0;
                            }
                            if (!isset($event->type)) {
                                $event->type = 0;
                            }
                            if (!isset($event->targetX)) {
                                $event->targetX = '0';
                            }
                            if (!isset($event->targetY)) {
                                $event->targetY = '0';
                            }
                            if (!isset($event->targetClick)) {
                                $event->targetClick = '';
                            }
                            if (!isset($event->targetDrop)) {
                                $event->targetDrop = '';
                            }
                            if (!isset($event->datetime)) {
                                $event->datetime = '';
                            }

                            $gamebookService->newEvent($matchId, $event);
                        }
                    }
                }

                $importModel->extracted = 8;
                $importModel->save();

                //DB::commit();
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
                //DB::rollback();
            }
        }

        $response['message'] = 'Importação finalizada';

        if (!empty($errors)) {
            $response['errors'] = $errors;
        }
        return $this->response->json($response);
    }


    /**
     * Extrai os dados da tabela "MATCHJSON" e separa os dados nas tabelas separadas
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function extractMatches()
    {
        $errors = [];

        $gamebookService = new GamebookService();

        $matches = (new MatchJson())->toExtract()->get();

        if (!$imports) {
            return $this->response->json(['message' => 'Nada para importar']);
        }

        foreach ($imports as $importModel) {

            //DB::beginTransaction();
            try {
                $matches = $this->transformStringToJson($importModel->payload);
                                //$matches = $this->transform($importModel->payload);

                $matches = (new MatchJson())->toExtract()->get();


                foreach ($matches as $k => $match) {
                    $match =  json_decode($match->payload);
                    $matchExists = $gamebookService->getMatchById($match->gameSessionId);

                    try{
                        $matchJson = new MatchJson();
                        $matchJson->sessionId = $match->gameSessionId;
                        $matchJson->datetime  = $match->datetime;
                        $matchJson->payload = json_encode($match);
                        $matchJson->obs = 'import : '. $importModel->id;
                        $matchJson->saveOrFail();

                    } catch (\Exception $e) {
                        if($e->getCode() == 23000) {
                            $importModel->extracted = -2;
                            $importModel->comments .= " duplicado";
                            $importModel->save();
                        }
                        //continue 2;
                    }

                    if ($matchExists) {
                        $comment ='';
                        if($matchExists->errors != $match->totalErrors) {
                            $comment = 'ERRO ' . $matchExists->errors .' != ' . $match->totalErrors;
                            $errors[] = ['error' => 'ERRO ' . $matchExists->errors .' != ' . $match->totalErrors , 'obj' => $importModel->id];
                        }
                        if($matchExists->hits != $match->totalHits) {
                            $comment .= 'ACERTO ' . $matchExists->hits .' != ' . $match->totalHits;
                            $errors[] = ['error' => 'ACERTO ' . $matchExists->hits .' != ' . $match->totalHits , 'obj' => $importModel->id];
                        }
                        if($comment != '') {
                            $importModel->comments = $comment;
                            $importModel->extracted = 4;
                            $importModel->save();
                        }

                        $matchExists->errors = $match->totalErrors;
                        $matchExists->hits = $match->totalHits;

                        $matchExists->save();

                        //dd($matchExists->errors);
                        //dd($match->totalErrors);

                        continue; //evita que partidas sejam salvas 2 vezes em imports q falharam
                    }

                    $playerId = $gamebookService->playerExists($match->playerName);

                    if (is_null($playerId)) {
                        $playerId = $gamebookService->newPlayer($match);

                        if ($playerId == null) {
                            throw new \Exception('Id do Jogador não informado!' . $k);
                        }
                    }

                    $matchId = $gamebookService->newMatch($playerId, $match);

                    if ($match->gameEvents) {
                        foreach ($match->gameEvents as $event) {
                            /*if (!isset($event->targetClick)) {
                                $event->targetClick = '';
                            }*/
                            if (!isset($event->category)) {
                                $event->category = 0;
                            }
                            if (!isset($event->type)) {
                                $event->type = 0;
                            }
                            if (!isset($event->targetX)) {
                                $event->targetX = '0';
                            }
                            if (!isset($event->targetY)) {
                                $event->targetY = '0';
                            }
                            if (!isset($event->targetClick)) {
                                $event->targetClick = '';
                            }
                            if (!isset($event->targetDrop)) {
                                $event->targetDrop = '';
                            }
                            if (!isset($event->datetime)) {
                                $event->datetime = '';
                            }

                            $gamebookService->newEvent($matchId, $event);
                        }
                    }
                }

                $importModel->extracted = true;
                $importModel->save();

                //DB::commit();
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
                //DB::rollback();
            }
        }

        $response['message'] = 'Importação finalizada';

        if (!empty($errors)) {
            $response['errors'] = $errors;
        }
        return $this->response->json($response);
    }

    protected function transform($payload)
    {
        if ($payload == '{"data": []}') {
            return false;
        }

        $payload = str_replace(['{"data": ['], '', $payload);

        $payload = rtrim($payload, ']}');
        $payload = explode('#', $payload);
        foreach ($payload as $k => $p) {
            $payload[$k] = json_decode($p);
        }
        return $payload;
    }

    /**
     * le da tabela de metricas e importa para as tabelas separadas
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function metrics()
    {
        $gamebookService = new GamebookService();


        /*foreach($playersModel as $p){
            $players[$p->id] = $p->playerCreatedAt;
        }*/

        $metricsData = (new GamebookMetrics())->all();
        if ($metricsData) {
            foreach ($metricsData as $metricsJson) {
                //  var_dump($metricsJson->json);
                $metrics = json_decode($metricsJson->json);
                if ($metrics == null) continue;

                if (!isset($metrics->playerCreatedAt)) {
                    $metrics->playerCreatedAt = '';
                }

                $playerHash = $metrics->playerName;//. $metrics->playerCreatedAt;
                $playerId = $gamebookService->playerExists($playerHash);

                if (is_null($playerId)) {
                    $playerId = $gamebookService->newPlayer($metrics);
                }
                if ($playerId == null) {
                    var_dump($metrics);
                    echo 'OPS';
                }
                $matchId = $gamebookService->newMatch($playerId, $metrics);

                if ($metrics->gameEvents) {
                    foreach ($metrics->gameEvents as $event) {
                        $gamebookService->newEvent($matchId, $event);
                    }
                }

                //$gamebookService->newMatch($playerId, $metrics);


                $data[] = [
                    'player' => $metrics->playerName,
                    'data' => $metrics->playerCreatedAt
                ];
            }
        }

        return $this->response->json($data);
    }

    public function executiveFunctions()
    {
        $minigame = Input::get('minigame');
        $level = Input::get('level');

        $data = (new Gamebook())->getExecutiveFunctions($minigame, $level);
        return $this->response->json($data);
    }

    public function gameProgress($name)
    {

        //$name = Input::get('name');
        $minigame = Input::get('minigame');
        $level = Input::get('level');
        $variavel = Input::get('variavel');

        $data = (new Gamebook())->getGameProgress($name, $minigame, $level, $variavel);
        return $this->response->json($data);
    }

    public function gameProgressDistinct($name)
    {

        //$name = Input::get('name');
        $minigame = Input::get('minigame');
        $level = Input::get('level');

        $data = (new Gamebook())->getGameProgressDistinct($name, $minigame, $level);
        return $this->response->json($data);
    }

    public function playedByLevel($name)
    {

        //$name = Input::get('name');
        $minigame = Input::get('minigame');

        $data = (new Gamebook())->getPlayedByLevel($name, $minigame);

        return $this->response->json($data);
    }

    //corrige os erros repetidos
    public function fixRepeatedErrors()
    {
        $service = new GamebookStatsService();
         return $service->updateRepeatedErros();
    }


}
