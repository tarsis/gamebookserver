<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Jan 2017 17:34:55 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GbResponsavel
 * 
 * @property int $ID_Responsavel
 * @property string $Nome
 * @property string $Instituicao
 * 
 * @property \Illuminate\Database\Eloquent\Collection $gb_usuarios
 *
 * @package App\Models
 */
class GbResponsavel extends Eloquent
{
	protected $table = 'gb_responsavel';
	protected $primaryKey = 'ID_Responsavel';
	public $timestamps = false;

	protected $fillable = [
		'Nome',
		'Instituicao'
	];

	public function gb_usuarios()
	{
		return $this->hasMany(\App\Models\GbUsuario::class, 'ID_Responsavel');
	}
}
