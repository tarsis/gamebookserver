<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Jan 2017 17:34:55 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GbUsuario
 * 
 * @property int $ID_Usuario
 * @property string $Nome
 * @property int $Idade
 * @property string $Cidade
 * @property string $Pais
 * @property int $ID_Responsavel
 * 
 * @property \App\Models\GbResponsavel $gb_responsavel
 * @property \Illuminate\Database\Eloquent\Collection $gb_minigames
 *
 * @package App\Models
 */
class GbUsuario extends Eloquent
{
	protected $table = 'gb_usuario';
	protected $primaryKey = 'ID_Usuario';
	public $timestamps = false;

	protected $casts = [
		'Idade' => 'int',
		'ID_Responsavel' => 'int'
	];

	protected $fillable = [
		'Nome',
		'Idade',
		'Cidade',
		'Pais',
		'ID_Responsavel'
	];

	public function gb_responsavel()
	{
		return $this->belongsTo(\App\Models\GbResponsavel::class, 'ID_Responsavel');
	}

	public function gb_minigames()
	{
		return $this->hasMany(\App\Models\GbMinigame::class, 'ID_Usuario');
	}
}
