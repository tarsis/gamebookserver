<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Jan 2017 17:34:55 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GbMinigame
 * 
 * @property int $ID_Minigame
 * @property string $Nome
 * @property int $Nivel
 * @property int $Score
 * @property int $Estrelas
 * @property int $Max_Score
 * @property int $Data
 * @property int $Hora
 * @property int $ID_Usuario
 * 
 * @property \App\Models\GbUsuario $gb_usuario
 * @property \App\Models\GbMgFE $gb_mg_f_e
 *
 * @package App\Models
 */
class GbMinigame extends Eloquent
{
	protected $table = 'gb_minigame';
	protected $primaryKey = 'ID_Minigame';
	public $timestamps = false;

	protected $casts = [
		'Nivel' => 'int',
		'Score' => 'int',
		'Estrelas' => 'int',
		'Max_Score' => 'int',
		'Data' => 'int',
		'Hora' => 'int',
		'ID_Usuario' => 'int'
	];

	protected $fillable = [
		'Nome',
		'Nivel',
		'Score',
		'Estrelas',
		'Max_Score',
		'Data',
		'Hora',
		'ID_Usuario'
	];

	public function gb_usuario()
	{
		return $this->belongsTo(\App\Models\GbUsuario::class, 'ID_Usuario');
	}

	public function gb_mg_f_e()
	{
		return $this->hasOne(\App\Models\GbMgFE::class, 'ID_Minigame');
	}
}
