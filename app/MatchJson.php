<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchJson extends Model
{
    CONST STATUS_DUPLICADO = 1;
    CONST STATUS_SELECIONADO = 2;

    public $timestamps = false;
    public $table = 'matchjson';

    public function toExtract()
    {
        return $this->where('extracted', 0)->where('status', '<>',self::STATUS_DUPLICADO)->orderBy('status','desc')->orderBy('id','asc');
    }

    public function getUniquePlayerName()
    {
        return trim($this->playerName)."_".$this->age;
    }
}
