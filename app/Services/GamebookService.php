<?php
/**
 * Created by PhpStorm.
 * User: tarsis
 * Date: 18/03/17
 * Time: 15:53
 */

namespace App\Services;

use App\Models\GbEvent;
use App\Models\GbMatch;
use App\Models\GbPlayer;

class GamebookService
{
    public $players = [];

    public function __construct()
    {
        $this->getPlayers();

    }

    public function getMatchById($id) {
        return GbMatch::where('game_session_id', $id)->first();
    }

    public function getPlayers()
    {
        $playersModel = GbPlayer::all();
        foreach ($playersModel as $p) {
            $this->players[$p->id] = trim($p->player_name)."_". $p->age;
        }
    }

    /**
     * busca no cache de jogasdores
     *
     * @param $playerName combinacao 'nome_idade'
     * @return false|int|null|string
     */
    public function playerExists($playerName)
    {
        if (!in_array($playerName, $this->players)) {
            return null;
        }

        return array_search($playerName, $this->players);
    }

    /** Cria um novo jogador e adiciona ao cache
     *
     * @param $metrics
     * @return int
     * @throws \Exception
     */
    public function newPlayer($metrics)
    {
        $player = new GbPlayer();
        $player->player_name = trim($metrics->playerName);
        $player->age = $metrics->age;
        $player->entity = 'UNE';
        if(isset($metrics->uniqueIdDevice)){
            $player->comentarios = $metrics->uniqueIdDevice;
        }

        if (empty($metrics->playerCreatedAt)) {
            $metrics->playerCreatedAt = time();
        }
        $player->created_at = $metrics->playerCreatedAt;
        try{
          $player->saveOrFail();
        }catch(\Exception $e) {
          sort($this->players);
          dd($player->player_name,$this->players);
          throw $e;
        }


        //$this->addPlayer($player);

        $this->players[$player->id] = $player->player_name ."_". $player->age;
        return $player->id;
    }

    public function addPlayer(GbPlayer $player)
    {
        $this->players[$player->id] = $player->player_name;
    }

    public function newMatch($playerId, $importId, $matchData)
    {
        //echo '<pre>'.var_dump($matchData);

        $match = new GbMatch();
        $match->import_id = $importId;
        $match->game_session_id = $matchData->gameSessionId;
        $match->player_id = $playerId;
        $match->minigame_id = $matchData->minigame;
        $match->level_id = $matchData->level;
        $match->datetime = $matchData->datetime;
        $match->duration = $matchData->duration;
        $match->errors = $matchData->totalErrors;
        $match->hits = $matchData->totalHits;
        $match->omissions = $matchData->totalOmissions;
        $match->repeated_errors = $matchData->totalRepetitionErrors;
        $match->total_eventos = count($matchData->gameEvents);

        $match->save();
        return $match->id;
    }

    public function newEvent($matchId, $eventData)
    {
        if(is_null($eventData->targetClick)){
            $eventData->targetClick = 'NULL';
        }

        if(isset($eventData->datetime)) {
          //throw new \Exception("Existe dois tipos : 'datetime' e 'dateTime' {$eventData->datetime}");
        }

        if(isset($eventData->dateTime)) {
          $eventData->datetime  = $eventData->dateTime;
        }

        $event = new GbEvent();
        $event->match_id = $matchId;
        $event->category = $eventData->category;
        $event->type = $eventData->type;
        $event->name = $eventData->eventName;
        $event->click_target = $eventData->targetClick;
        $event->drop_target = $eventData->targetDrop;
        $event->target_x = $eventData->targetX;
        $event->target_y = $eventData->targetY;
        $event->datetime = str_pad($eventData->datetime, 25, "0");
        $event->object_id = $eventData->objectId;

        $types = [
            GbEvent::TYPE_GAME_SHOWED_TARGET,
            GbEvent::TYPE_ERROR,
            GbEvent::TYPE_HIT,
            GbEvent::TYPE_GAME_OMISSION
        ];

        if(in_array($event->type, $types)) {
            $event->target_size = $event->getTargetSize();
        }

        //$event->created_at =

        $event->save();

        return $event->id;
    }
}
