<?php
/**
 * Created by PhpStorm.
 * User: tarsis
 * Date: 18/03/17
 * Time: 15:53
 */

namespace App\Services;

use App\Models\GbEvent;
use App\Models\GbMatch;
use App\Models\GbPlayer;
use App\Models\Player;
use Carbon\Carbon;
use function Couchbase\defaultDecoder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class EventService
{
    public $players = [];
    public $semObj =0;
    public $comObj =0;

    const MINIGAME_AVALIADO = 6;

    public function __construct()
    {

    }

    /**
     * Retorna nome do grupo a que pertence um valor no range definido
     * @param $col
     * @param $min
     * @param $max
     * @param $steps
     * @param $value
     * @return string
     * @throws \Exception
     */
    public function getGroupFromRange($col, $min, $max, $steps, $value)
    {
        $range = range($min, $max, $steps);
        $count = count($range) - 2;

        if ($value < $min)
            throw new \Exception('FORA DO RANGE');

        for ($i = 0; $i <= $count; $i++) {
            if ($value >= $range[$i] and $value < $range[$i+1]) {
                return $col.'_'.$range[$i].'_'.$range[$i+1];
            }
        }

        return $col.'_'.$range[$i].'_mais';
    }

    public function testRange()
    {
        $range = range(0, 45, 1);
        foreach($range as $valor) {
            echo $this->getGroupFromRange('hits', 0, 40,4, $valor) . PHP_EOL;
        }
    }

    public function jogadoresAptos()
    {
        return GbPlayer::where('excluir', 0)->orWhere('excluir', null)->pluck('id');
    }

    public function minigamesAptos()
    {
        return GbMatch::where('apto', 1)
            //->where('id', '=', 2017)
            //->where('import_id', '>',1730)
            ->with('player')
            ->orderBy('id','desc')->get();
    }

    public function atualizaPartidasAptas()
    {
        $jogadores = $this->jogadoresAptos();

        DB::table('match')->update(['apto' => 0]); //zera tudo

        GbMatch::where('minigame_id', EventService::MINIGAME_AVALIADO)
            ->whereIn('player_id', $jogadores)
            ->update(['apto' => 1]);
    }


    public function atualizaRegistrosSemObjetctId($minigames)
    {
        echo count($minigames).' minis' . PHP_EOL;
        foreach($minigames as $m) {
            $events = GbEvent::where('match_id', $m->id)
                ->whereRaw('target_x = object_id')
                ->whereNotNull('target_x')->orderBy('datetime')->get();
            echo count($events).' Eventos, jogador '.$m->player->player_name . PHP_EOL;

            foreach($events as $k => $event) {
                if($k==0) {
                    $m->tem_objetos = 0;
                    $m->save();
                }
                $event->object_id = null;
                $event->save();

                echo 'obj ' .$event->match_id.'::'.$event->object_id.' target'. $event->target_x. PHP_EOL;
                //$event->object_id = $event->target_x;
                //$event->save();
            }
            //break;
        }
    }

    public function atualizaTempoResposta($minigames)
    {
        //echo count($minigames).' minig' . PHP_EOL;
        foreach($minigames as $m) {
            $events = GbEvent::where('match_id', $m->id)->orderBy('datetime')->get();
            echo "Partida $m->id - ".count($events).' Eventos, jogador '.$m->player->player_name . PHP_EOL;
            $this->tempoDeResposta($events);
            //break;
        }
    }

    /**
     * @deprecated
     */
    public function ajustaSegundosInteiro() {
        $minigames = $this->minigamesAptos();
        $c = count($minigames);
        echo count($minigames).' minig' . PHP_EOL;
        foreach($minigames as $k=> $m) {
            echo $k.' minig' .' de '.$c. PHP_EOL;
            $events = GbEvent::where('match_id', $m->id)->orderBy('datetime')->get();
            foreach ($events as $event) {

                $event->second_int = (int) $event->seconds;
                $event->save();
              //  dd($event->toArray());
            }
            //break;
        }
    }

    public function atualizaOmissao($events)
    {
        /** @var GbEvent[] $events */
        $events = $events->filter(function($item) {
            $eventosAnalisados = [
                GbEvent::TYPE_GAME_SHOWED_TARGET,
                GbEvent::TYPE_HIT,
                GbEvent::TYPE_ERROR,
                GbEvent::TYPE_GAME_OMISSION
            ];

            return in_array($item->type, $eventosAnalisados);
        });


        $match = GbMatch::find($events->first()->match_id);

        $alvos = explode('|',$match->alvo);

        $qtdOuts = 0;

        foreach ($events as $k=> $event) {

            echo 'ALVO SAIU' .PHP_EOL;
            if ($event->type == GbEvent::TYPE_GAME_OMISSION) {
                $alvo = $event->getTargetName();
                if (!in_array($alvo, $alvos)) {
                    $qtdOuts++;
                }
            }
        }
        $match->omissions = $qtdOuts - $match->hits;
        $match->save();
    }

    /**
     * Calcula tempo de resposta ignorando mes 5
     *
     * @param GbEvent $events[]
     * @throws \Exception
     */
    public function tempoDeResposta($events)
    {
        $sleep = false;
        $apareceu = [];
        $eventos = [];

        $alvo = null;

        $alvos = [];

        /** @var GbEvent[] $events */
        $events = $events->filter(function($item) {
            $eventosAnalisados = [
                GbEvent::TYPE_GAME_SHOWED_TARGET,
                GbEvent::TYPE_HIT,
                GbEvent::TYPE_ERROR,
                GbEvent::TYPE_GAME_OMISSION
            ];

            return in_array($item->type, $eventosAnalisados);
        });


        $match = GbMatch::find($events->first()->match_id);
        $pular = [4752, 4746];
        echo '>>' .$match->player_id;
        $pularJogador = [179, 176];
        if( in_array($match->player_id ,$pularJogador) OR in_array($match->id, $pular)) {
            echo '>>' .$match->player_id;
            return; // pulando provisoriamente
        }

       // $this->semObj = 0;
       // $this->comObj = 0;

        // coleta os nomes de alvos que tem evento SHOWED_TARGET .. necessario para calcular o tempo de resposta
        foreach ($events as $k=> $event) {
            $mes  =substr($event->datetime,5, 2);
            if($mes < 5) {
                throw new \Exception("MES < 5 event_id " .$event->id);
            }


            if ($event->type == GbEvent::TYPE_GAME_SHOWED_TARGET) {

                if($event->name == 'Alvo apareceu') {

                    $alvo = $event->getTargetName();
                    if(!in_array($alvo, $alvos)) {
                        $alvos[] = $alvo;
                        echo 'Definiu alvo '.$alvo.' -' .PHP_EOL;
                    }
                } else {
                    if($match->alvo_exclui_erros == 1){
                    //    echo ('@@@@#########Apareceu alvo com nome diferente '.$event->name).PHP_EOL;
                    }
                    //echo ('##########Apareceu alvo com nome diferente '.$event->name).PHP_EOL;
                }
            }
        }

        $match->alvo = implode('|', $alvos);
        $match->save();

        if($event->match_id == 4429 && $sleep) {
            sleep(1);
            echo ' ------------------ '.PHP_EOL;
        }
        //echo $events[$k]->datetime. " sem ". $this->semObj . ' - com '. $this->comObj .PHP_EOL;
    //            return;
        foreach ($events as $k=> $event) {
            $event->target_size = $this->getTargetSize($event);
            $event->save();

            if($event->match_id == 4429 && $sleep) {
                sleep(1);
                echo "F::: " .implode(' - ', $apareceu) . PHP_EOL;
            }

            if($event->match_id = 2017) {
                dd($event->object_id, str_contains('.', $event->object_id));
            }

            echo 'Show' . PHP_EOL;
            if ($event->type == GbEvent::TYPE_GAME_SHOWED_TARGET) {

                $alvo = $event->getTargetName();
                if ("Nao Alvo apareceu" != $event->name) {

                    // && $match->alvo_exclui_erros == 0
                    if (!in_array($alvo, $alvos)) {
                        dd([$event->toArray(),$alvo, $alvos, 'Alvo nao encontrado na lista de alvos']);
                    }
                }


                if (true === array_search($event->click_target, $apareceu)) {
                     dd('JA APARECEU ', [$event->click_target, $apareceu]);
                    //    if(empty($event->object_id))
                    //       throw new \Exception('VAZIO');
                    //   throw new \Exception('Ja existe');
                }


                //if($alvo == null) {
                //    if($event->name == 'Alvo apareceu') {
                //   $alvo = explode(' ', $event->click_target)[0];
                //       echo 'Definiu alvo '.$alvo.' -' .PHP_EOL;
                //  }
                //}

                if(array_key_exists($event->object_id, $apareceu)) {
                    dd("ja tinha aparecido antes");
                }

                $apareceu[$event->object_id] = $event->click_target;
                $eventos[$event->object_id] = $event;

            //    echo $event->click_target . PHP_EOL;
                if($event->match_id == 4429 && $sleep) {
                    sleep(1);
                    echo "F::: " .implode(' - ', $apareceu) . PHP_EOL;
                }

                continue;
            }



            /*$allowed = [
                GbEvent::TYPE_HIT,
                GbEvent::TYPE_ERROR,
                GbEvent::TYPE_GAME_OMISSION
            ];*/

            if ($event->type == GbEvent::TYPE_HIT || $event->type == GbEvent::TYPE_ERROR) {
                //dd([$apareceu, $k]);
                $nomeTipo = (GbEvent::TYPE_HIT) ? 'HIT': 'ERROR';
                echo $nomeTipo . PHP_EOL;

                $tipo = array_key_exists($event->object_id, $apareceu);
                if ($match->tem_objetos == 0) {
                    $tipo = array_search($event->click_target, ($apareceu));
                }

                if($event->id ==185301) {
                    echo var_dump($apareceu);
                    echo var_dump($eventos);
                }

                if (false === $tipo) {

                    if($event->type == GbEvent::TYPE_HIT) {
                        // if($event->id ==185300) {
                        //dd([$tipo, $tipo2]);
                        //}


                        dd(['NAO APARECEU','event '. $event->id, 'match '.$event->match_id, $alvos, $event->click_target, $apareceu, $event->object_id]);
                        throw new \Exception('Evento de acerto não registrou SHOWED_TARGET'.' EV  '.$event->id.' mtc '.$event->match_id);
                    } else {
                        //$match->alvo_exclui_erros = 1;
                       ;; $match->save();
                    }
                    continue;
                }

                dd($event);
                $event->response_time = $event->seconds - $eventos[$event->object_id]->seconds;
                $event->save();
                //if(false !== array_search($event->click_target, $apareceu)) {
                //   if(empty($apareceu)) throw new \Exception('Sumiu sem aparecer');
                echo $nomeTipo.' - ' . $event->seconds . ' -' . $eventos[$event->object_id]->seconds .' -- '. $event->click_target ." ev {$event->id}". PHP_EOL;
                //}
                if ($event->match_id < 210) {  ///WHAT? nao contava omissao eu acho
                    throw new \Exception('menor q 210');
                    unset($apareceu[$event->object_id]);
                    unset($eventos[$event->object_id]);
                }

            }


            echo 'OMISS ALVO SAIU' .PHP_EOL;
            if ($event->type == GbEvent::TYPE_GAME_OMISSION) {
                //echo 'SAIU - ' . $event->click_target . PHP_EOL;
                //$tipo = array_search($event->click_target, $apareceu);
                $tipo = array_key_exists($event->object_id, $apareceu);


                if(false === $tipo) {

                    $alvo = $event->getTargetName();
                    if (!in_array($alvo, $alvos)) {
                     //   $match->alvo_exclui_erros = 1;
                      //  $match->save();
                    }

                    continue;
                }

                if ($match->alvo_exclui_erros) {
                    $alvo = $event->getTargetName();
                    if (!in_array($alvo, $alvos)) {
                        throw new \Exception('SUMIU SEM SER ALVO! BOMM');
                    }
                }

                $event->response_time = $event->seconds - $eventos[$event->object_id]->seconds;
                $event->save();

                echo 'SAIU - ' . $event->click_target . PHP_EOL;

                unset($apareceu[$event->object_id]);
                unset($eventos[$event->object_id]);

                continue;
            }



            if (!empty($apareceu)) {
                //var_dump([$event->match_id, $alvos, $event->click_target, $apareceu]);
                //throw new \Exception('Sobrou');
            }
        }
        //$event->eventos
    }

    public function getTargetSize(GbEvent $event)
    {
        return $event->getTargetSize();

  /*      if(false !== strpos($event->click_target, "pequen")) {
            return 1;
        };
        if(false !== strpos($event->click_target, "med")) {
            return 2;
        };
        if(false !== strpos($event->click_target, "grand")) {
            return 3;
        };
        return 4;*/
    }

    /**
     * @param $nomeAlvo nome do alvo que é falso acerto ex: Cacador
     * @param $evento
     */
    public function atualizaFalsoAcerto($nomeAlvo, $evento)
    {
        if($evento->getTargetName() != $nomeAlvo) {
            return ;
        }

        if($evento->type == GbEvent::TYPE_GAME_SHOWED_TARGET && $evento->name == 'Alvo apareceu') {
            $evento->falso_acerto = 1;
            $evento->name = 'Nao Alvo apareceu';
        }

        if($evento->type == GbEvent::TYPE_HIT && $evento->getTargetName() == $nomeAlvo) {
            $evento->falso_acerto = 1;
            $evento->type = GbEvent::TYPE_ERROR;

           // echo "" . $evento->getTargetName().PHP_EOL;
        }

        return $evento->save();
    }

    public function atualizaTempoRespostaComObjeto($events)
    {
        $eventsGroup = $events->filter(function($item) {
            return !empty($item->object_id);
        })->groupBy('object_id');


        $orfaos = $eventsGroup->filter(function ($item) {
            return $item->count() == 1;
        });

        $orfaos->map(function ($item) {
            $item[0]->evento_orfao = 1;
            $item[0]->save();
            return $item;
        });

        $eventsGroup->filter(function ($item) {
            return  $item->count() > 1;
        })->map(function ($itens) {
            $inicio = $itens->shift()->seconds;

            foreach ($itens as $item) {
                $item->response_time = $item->seconds - $inicio;
                $item->save();
            }
        });
    }

    public function atualizaOmissoesComObjeto($eventos)
    {
        $eventos->filter(function ($item){
            return !empty($item->object_id);
        })
            ->groupBy('object_id')
            ->filter(function($item) {
                return count($item) == 2;
            })->filter(function($itens) {
                $primeiro  = $itens->shift();
                $segundo  = $itens->shift();

                if ($primeiro->type != GbEvent::TYPE_GAME_SHOWED_TARGET) {
                    return false;
                    throw new \Exception('Primeiro nao é show');
                }
                if( $segundo->type == GbEvent::TYPE_GAME_OMISSION) {
                    $segundo->omissao = 1;
                    $segundo->save();
                }
                return $segundo->type == GbEvent::TYPE_GAME_OMISSION;
            });
    }
}
