<?php
/**
 * Created by PhpStorm.
 * User: tarsis
 * Date: 18/03/17
 * Time: 15:53
 */

namespace App\Services;

use App\Models\EventType;
use App\Models\GbEvent;
use App\Models\GbMatch;
use App\Models\GbPlayer;

class GamebookStatsService
{
    public $players = [];

    public $types = [];
    public $categories = [
        1 => 'CLICK',
        2 => 'DROP',
        3 => 'GAME_EVENT'
    ];

    public function __construct()
    {
        $this->getTypes();
    }

    public function getTypes()
    {
        $models = EventType::all();
        foreach ($models as $e) {
            $this->types[$e->id] = $e->name;
        }
    }

    public function getAllMatchesByPlayer($playerId, $minigame)
    {
        return;
    }

    public function getAllMatches($player, $minigame, $level, $column)
    {
        $match = Match::where('player_id', $player)
            ->where('minigame_id', $minigame)
            ->where('level_id', $level)
            ->get();
        return;
    }

    public function updateRepeatedErros()
    {
        $matches = GbMatch::all();
        if ($matches->count() == 0) {
            throw new \Exception('Nenhuma partida encontrada');
        }

        foreach ($matches as $match) {
            $events = GbEvent::where('match_id', $match->id)
                ->get();
           // $this->setSeconds($events);
            $this->updateAleatoryActions($match, $events);
            $this->updateRepeatedErrors($match, $events);

        }
    }

    private function setSeconds($events) {
        $event = $events->shift();
        $format = 'Y-m-d H:i:s.u';

        if(!str_contains($event->datetime,'.')) {
            //$event->datetime .= '.00000';
        }

        echo var_dump($event->datetime);

        $date =  \DateTime::createFromFormat('Y-m-d H:i:s.u', $event->datetime);
        //var_dump($date);
        echo $date->format($format) .'<br>';
        foreach ($events as $k => $event) {
            //echo $k .'<br>';
            //$dateEvent =  \DateTime::createFromFormat('Y-m-d H:i:s.u', $event->datetime);

            if(!str_contains($event->datetime,'.')) {
        //        $event->datetime .= '.02000';
            }

            $dateEvent =  new \DateTime($event->datetime);
            $sub = $dateEvent->diff($date);
            //$sub = $sub->format('i:s.u');
            $sub = $this->mdiff($date, $dateEvent);
            echo ''  //$event->name
                . ' s '. $event->datetime
                . ' ev ' . $dateEvent->format('U.u')
                . ' sub ' . $sub . '<br>';
        }

        exit;
    }
    function mdiff($date1, $date2){
        //Absolute val of Date 1 in seconds from  (EPOCH Time) - Date 2 in seconds from (EPOCH Time)
        $diff = abs(strtotime($date1->format('i:s.u'))-strtotime($date2->format('d-m-Y H:i:s.u')));

        //Creates variables for the microseconds of date1 and date2
        $micro1 = $date1->format("u");
        $micro2 = $date2->format("u");

        //Absolute difference between these micro seconds:
        echo $micro = abs($micro1 - $micro2);

        //Creates the variable that will hold the seconds (?):
        $difference = $diff.".".$micro;

        return $difference;
    }


    private function updateAleatoryActions($match, $events)
    {
        $type = array_search('ALEATORY_ACTION', $this->types);
        $count = 0;
        foreach ($events as $k => $event) {
            if($event->type == $type) {
                $count++;
            }
        }

        if($count > 0) {
            $match->aleatory_actions = $count;
            $match->save();
            echo "ERROS ALEATORIOS {$match->id}: erros {$count} <br>";
        }
    }

    private function updateRepeatedErrors($match, $events)
    {
        $type_error = array_search('ERROR', $this->types);
        $repeated_errors = 0;
        foreach ($events as $k => $event) {
            if ($k ==0) continue;
            if($event->type != $type_error || $events[$k-1]->type != $event->type ) {
                continue;
            }
            if (
                $events[$k-1]->click_target == $event->click_target &&
                $events[$k-1]->drop_target == $event->drop_target &&
                $event[$k-1]->objectId == $event->objectId
            ) {
                $repeated_errors++;
            }
        }
        if($match->repeated_errors != $repeated_errors) {
            $match->repeated_errors = $repeated_errors;
            echo "Partida {$match->id}: erros {$match->repeated_errors} ; encontrados $repeated_errors <br>";
            $match->save();
        }
    }

    private function updateFirstMove($events)
    {
        $types = [1, 2]; // hit , error
        $moves = [];

        foreach ($events as $k => $event) {
            if(!in_array($event->type, $types)) {
                continue;
            }
            echo var_dump($event).'<br>';
            if(!isset($moves[$event->drop_target])) {
                $moves[$event->drop_target] = [];
            }

            if(!in_array($moves[$event->drop_target], $event->click_target)) {
                if($event->type == 'ERROR') {
                    $event->type = 'MOVE';
                    $event->description = 'Tentativa';
                    //$event->save();
                    echo 'alterou<br>';
                }
                $moves[$event->drop_target][] = $event->click_target;
            }
        }
    }
}
