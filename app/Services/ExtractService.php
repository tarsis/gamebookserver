<?php
/**
 * Created by PhpStorm.
 * User: tarsis
 * Date: 18/03/17
 * Time: 15:53
 */

namespace App\Services;

use App\Models\Import;
use App\Models\GbMatch;
use App\Models\GbEvent;
use App\MatchJson;
use Illuminate\Support\Facades\DB;
use App\Services\GamebookService;

class ExtractService
{
    public function __construct(GamebookService $gamebookService)
    {
      $this->gamebookService = $gamebookService;
    }

    public function extractImport($importModel)
    {
      $gamebookService = $this->gamebookService;

      $matches = $this->transformStringToJson($importModel,false);

      //$matches = $this->transform($importModel->payload);
      if (!$matches) {
        $importModel->extracted = Import::VAZIO;
        $importModel->save();
        return;
          //throw new \Exception('Sem partidas '.$importModel->id);
      }

      if($importModel->incomplete == true) {
        echo "INCOMPLETO - retirando ultima partida \n";
        array_pop($matches);
      }
      echo "extraindo partidas \n";
      if(!is_array($matches)) {
        dd($matches);
      }

      foreach ($matches as $k => $match) {

          $playerId = $this->savePlayerIfIsNew($match);

          // usar:?? $matchExists = $gamebookService->getMatchById($match->gameSessionId);

          try {
              $matchJson = new MatchJson();
              $matchJson->import_id = $importModel->id;
              $matchJson->sessionId = $match->gameSessionId;
              $matchJson->datetime  = $match->datetime;
              $matchJson->payload = json_encode($match);
              //$matchJson->obs = $importModel->id;
              //$matchJson->status = $matchExists ? 1: 2;
              $check = json_decode($matchJson->payload);

              if(empty($check) || is_null($check)) {
                  echo "OPS \n";
                  dd($check);
              }
              $matchJson->saveOrFail();

          } catch (\Exception $e) {
              if($e->getCode() == 23000) {
                  //$importModel->extracted = -2;
                  $importModel->comments .= " duplicado";
                //  throw $e;
                  //$importModel->save();
              } else{
                  throw $e;
              }
          }
      }

      $importModel->extracted = true;
      $importModel->save();
    }


    public function extractImportXXXXXXXXXXXXXXXXXXXXXXXXXX($importModel)
    {
      $gamebookService = $this->gamebookService;

      $matches = $this->transformStringToJson($importModel,false);

      //$matches = $this->transform($importModel->payload);
      if (!$matches) {
        $importModel->extracted = Import::VAZIO;
        $importModel->save();
        return;
          //throw new \Exception('Sem partidas '.$importModel->id);
      }
      if($importModel->incomplete == true) {
        echo "INCOMPLETO - retirando ultima partida \n";
        array_pop($matches);
      }
      echo "extraindo partidas \n";
      if(!is_array($matches)) {
        dd($matches);
      }

      foreach ($matches as $k => $match) {
          $search = trim($match->playerName)."_".$match->age;

          $playerId = $gamebookService->playerExists($search);

          if (is_null($playerId)) {
              $playerId = $gamebookService->newPlayer($match);

              if ($playerId == null) {
                  throw new \Exception('Id do Jogador não informado!' . $k);
              }
          }

          $matchExists = $gamebookService->getMatchById($match->gameSessionId);

          try{
              $matchJson = new MatchJson();
              $matchJson->import_id = $importModel->id;
              $matchJson->sessionId = $match->gameSessionId;
              $matchJson->datetime  = $match->datetime;
              $matchJson->payload = json_encode($match);
              $matchJson->obs = $importModel->id;
              $matchJson->status = $matchExists ? 1: 2;
              $check = json_decode($matchJson->payload);

              if(empty($check) || is_null($check)) {
                  echo "OPS \n";
                  dd($check);
              }
              $matchJson->saveOrFail();

          } catch (\Exception $e) {
              if($e->getCode() == 23000) {
                  //$importModel->extracted = -2;
                  $importModel->comments .= " duplicado";
                //  throw $e;
                  //$importModel->save();
              } else{
                  throw $e;
              }

              //continue 2;
          }

          if ($matchExists) {
            //echo "Existe".$matchJson->sessionId;
              $comment ='';
              if($matchExists->errors != $match->totalErrors) {
                  $comment = 'ERRO ' . $matchExists->errors .' != ' . $match->totalErrors;
                  $errors[] = ['error' => 'ERRO ' . $matchExists->errors .' != ' . $match->totalErrors , 'obj' => $importModel->id, 'sid1' => $matchExists->gameSessionId, 'sid2' => $match->gameSessionId];
              }
              if($matchExists->hits != $match->totalHits) {
                  $comment .= 'ACERTO ' . $matchExists->hits .' != ' . $match->totalHits;
                  $errors[] = ['error' => 'ACERTO ' . $matchExists->hits .' != ' . $match->totalHits , 'obj' => $importModel->id, 'sid1' => $matchExists->gameSessionId, 'sid2' => $match->gameSessionId];
              }
              if($comment != '') {
                  $importModel->comments .= $comment;

                  //$importModel->extracted = 4;
                  //$importModel->save();
              }

              //$matchExists->errors = $match->totalErrors;
              //$matchExists->hits = $match->totalHits;

//                        $matchExists->save();

              //dd($matchExists->errors);
              //dd($match->totalErrors);

              //continue; //evita que partidas sejam salvas 2 vezes em imports q falharam
          }


          $matchId = $gamebookService->newMatch($playerId, $importModel->id, $match);

          if ($match->gameEvents) {
              foreach ($match->gameEvents as $i => $event) {
                  /*if (!isset($event->targetClick)) {
                      $event->targetClick = '';
                  }*/
                  if(!isset($event->eventName)) {
                    echo "cortado import {$importModel->id}: total eventos ".count($match->gameEvents).", evento: ".($i+1) . "\n";
                    $event->eventName = "CORTADO";
                  }

                  if (!isset($event->category)) {
                      $event->category = 0;
                  }
                  if (!isset($event->type)) {
                      $event->type = 0;
                  }
                  if (!isset($event->targetX)) {
                      $event->targetX = '0';
                  }
                  if (!isset($event->targetY)) {
                      $event->targetY = '0';
                  }
                  if (!isset($event->targetClick)) {
                      $event->targetClick = '';
                  }
                  if (!isset($event->targetDrop)) {
                      $event->targetDrop = '';
                  }
                  if (!isset($event->datetime)) {
                      $event->datetime = '';
                  }

                  $gamebookService->newEvent($matchId, $event);
              }
          }
      }

      $importModel->extracted = true;
      $importModel->save();
    }


    public function getUniquePlayerName($match)
    {
        return trim($match->playerName)."_".$match->age;
    }

    public function savePlayerIfIsNew($match)
    {
      $search = $this->getUniquePlayerName($match);
      $playerId = $this->gamebookService->playerExists($search);

      if (is_null($playerId)) {
          $playerId = $this->gamebookService->newPlayer($match);

          if ($playerId == null) {
              throw new \Exception('Id do Jogador não informado!' . $k);
          }
      }
      return $playerId;
    }

    public function extractMatchJson($matchJson)
    {
      $gamebookService = $this->gamebookService;
      $match =  json_decode($matchJson->payload);

          $playerId = $this->savePlayerIfIsNew($match);
          $matchExists = $gamebookService->getMatchById($match->gameSessionId);

          if($matchExists) {
              throw new \Exception("Partida já existe");
          }

          $matchId = $gamebookService->newMatch($playerId, $matchJson->import_id, $match);

          if(!$match->gameEvents) {
              throw new \Exception("Partida sem eventos.");
          }

          foreach ($match->gameEvents as $i => $event) {
            if(!isset($event->eventName)) {
                throw new \Exception('Evento cortado');
            }

            $this->gamebookService->newEvent($matchId, $event);
          }

      $matchJson->extracted = true;
      $matchJson->save();
    }

    /**
     * Transforma string em objeto json, normalizando os dados
     * @param $payload
     * @return bool
     */
    protected function transformStringToJson($import, $fixCut = false)
    {
        $payload= $import->payload;

        //if(count($payload) == 65535) {
        //  $import->extracted = Import::CORTADO;
        //}

        $pos = strpos($payload, 'Build');
        if ($pos !== false) {
            $import->comments .= substr($payload, $pos, 20);
        }

        $payload = $this->fixJsonSeparator($payload);

        if ($payload == '{"data": []}') {
            $import->extracted = Import::VAZIO;
            $import->save();
            return false;
        }

        $metrics = json_decode($payload);
        if ($metrics == null || !isset($metrics->data) || empty($metrics->data)) {

            $payload .= '"}]}]}';
            $metrics = json_decode($payload);
            if ($metrics == null || !isset($metrics->data) || empty($metrics->data)) {

              $import->extracted = Import::JSON_INVALIDO;
              $import->save();
              return false;
            }
        }
        $import->payload = $payload;
        $import->qtd_matches = count($metrics->data);

        $import->save();

        return $metrics->data;
    }

    protected function fixJsonSeparator($payload)
    {
              $payload = str_replace(', version: Build version: 6.1', '', $payload);
              $payload = str_replace('}#{', '},{', $payload);
              $payload = str_replace('#', '', $payload);
              return $payload;
    }

      /**
      * Muda status dos payloads identicos
      */
      public function fixDuplicatedImports()
      {
          $imports = Import::selectRaw('id, count(payload), payload')->groupBy('payload')
          ->havingRaw('count(payload) > 1')->orderBy('id','desc')->get();

          $ids = collect($imports)->map(function($item, $k) {
              return $item->id;
          });

          $payloads = collect($imports)->map(function($item) {
              return $item->payload;
          });

          $toChange = Import::whereIn('payload', $payloads)->whereNotIn('id', $ids)->get();

          $toChange->map(function ($item) {
              $item->extracted = ($item->payload == '{"data": []}') ? Import::VAZIO: Import::DUPLICADO;
              $item->save();
          });

          return $toChange->count();
      }

    /**
       *   Aponta payloads repetidos deixando apenas 1 ativo
       */
      public function xxxxxfixDuplicatedMatchesJson()
      {
          $imports = MatchJson::selectRaw('id, count(payload), payload')->groupBy('payload')
          ->havingRaw('count(payload) > 1')->orderBy('id','desc')->get();
          //$imports = DB::select('SELECT id, count(payload), payload FROM matchjson GROUP BY payload HAVING COUNT(payload) > 1 order by id desc');
          $ids = collect($imports)->map(function($item, $k) {
              $item->status = MatchJson::STATUS_SELECIONADO;
              $item->save();
              return $item->id;
          });

          $payloads = collect($imports)->map(function($item) {
              return $item->payload;
          });

          $toChange = MatchJson::whereIn('payload', $payloads)->whereNotIn('id', $ids)->get();

          $toChange->map(function ($item) {
              $item->status = MatchJson::STATUS_DUPLICADO;
              $item->save();
          });

          return $toChange->count();
      }

    /**
       *   Aponta payloads que foram salvos antes da hora
       */
      public function fixDuplicatedMatchesJson()
      {

          //todos com sessionId duplicado e ordena pelo tamanho
          //do payload maior para cada sessao
          /*$imports = MatchJson::selectRaw('id, sessionId, count(sessionId) as counted, MAX(length(payload)) as payloadSize')
               ->groupBy('sessionId')
               ->havingRaw('counted > 1 and sessionId = "bdadf08ffc399414bfab76f111478c50"')

               ->orderBy('sessionId','ASC')
               ->orderBy('payloadSize', 'DESC')->get();*/

          $imports = MatchJson::selectRaw('id, sessionId, count(sessionId) as counted, length(payload) as payloadSize')
                ->where('import_id' , '>', 1730)
              ->groupBy('sessionId')
              ->havingRaw('counted > 1')
          ->get();

          $imports->map(function($item){
              $duplicateds = MatchJson::selectRaw('id, sessionId, length(payload) as payloadSize')
                  ->where('sessionId', $item->sessionId)
                  ->orderBy('payloadSize', 'DESC')
                  ->get();

              foreach ($duplicateds as $k =>$item) {
                    $status = ($k == 0) ? MatchJson::STATUS_SELECIONADO : MatchJson::STATUS_DUPLICADO;
                    $item->status = $status;
                    $item->save();
              }
          });

          return $imports->count();

          dd($imports->toArray());
          //$imports = DB::select('SELECT id, payload. FROM matchjson GROUP BY payload HAVING COUNT(payload) > 1 order by id desc');
          collect($imports)->map(function($item, $k) {
              $item->status = MatchJson::STATUS_DUPLICADO;
              $item->save();
              return $item->id;
          });


          $ids = collect($imports)->map(function($item, $k) {
              $item->status = MatchJson::STATUS_SELECIONADO;
              $item->save();
              return $item->id;
          });
dd($imports);
          $sessionIds = collect($imports)->map(function($item) {
              return $item->sessionId;
          });

          $toChange = MatchJson::whereIn('sessionId', $sessionIds)->whereNotIn('id', $ids)->get();

          $toChange->map(function ($item) {
              $item->status = MatchJson::STATUS_DUPLICADO;
              $item->save();
          });

          return $toChange->count();
      }

      public function updateEventSeconds($match)
      {
              $events = GbEvent::where('match_id', $match->id)->orderBy('datetime')->get();
              $this->setEventSeconds($events);
      }

      private function setEventSeconds($events) {
          $event = $events->shift();
          $format = 'Y-m-d H:i:s.u';

          if(!str_contains($event->datetime,'.')) {
              //$event->datetime .= '.00000';
              throw new Exception('Evento nao possui final de microsegundos');
          }

          //echo var_dump($event->datetime);

          $date =  \DateTime::createFromFormat('Y-m-d H:i:s.u', $event->datetime);
          //var_dump($date);
          $start =  $date->format('U.u') .'\n';

          foreach ($events as $k => $event) {
              //echo $k .'<br>';

              //$dateEvent =  \DateTime::createFromFormat('Y-m-d H:i:s.u', $event->datetime);

              if(!str_contains($event->datetime,'.')) {
          //        $event->datetime .= '.02000';
                  throw new Exception('Evento nao possui final de microsegundos ' . $event->id);
              }

              $dateEvent =  \DateTime::createFromFormat('Y-m-d H:i:s.u', $event->datetime);
              $current = $dateEvent->format('U.u');
              $seconds = $current - $start;

              $event->seconds = $seconds;
              $event->second_int = (int) $seconds;
              $event->save();
              continue;

              $sub = $this->mdiff($date, $dateEvent);
              echo ''  //$event->name
                  . ' s '. $event->datetime
                  . ' ev ' . $dateEvent->format('U.u')
                  . ' sub ' . $sub . "\n";
          }

          //;exit;
      }

      private function mdiff($date1, $date2)
      {
          //Absolute val of Date 1 in seconds from  (EPOCH Time) - Date 2 in seconds from (EPOCH Time)
          $diff = abs(strtotime($date1->format('i:s.u'))-strtotime($date2->format('d-m-Y H:i:s.u')));

          //Creates variables for the microseconds of date1 and date2
          $micro1 = $date1->format("u");
          $micro2 = $date2->format("u");

          //Absolute difference between these micro seconds:
          $micro = abs($micro1 - $micro2);

          //Creates the variable that will hold the seconds (?):
          $difference = $diff.".".$micro;

          return $difference;
      }
}
