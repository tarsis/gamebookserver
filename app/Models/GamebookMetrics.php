<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Jan 2017 17:34:55 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GamebookMetrics
 *
 * @package App\Models
 */
class GamebookMetrics extends Eloquent
{
	protected $table = 'gamebookMetrics';
	public $timestamps = false;

	protected $fillable = [
		'id',
		'json'
	];
}
