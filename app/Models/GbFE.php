<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Jan 2017 17:34:55 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GbFE
 * 
 * @property int $ID_FE
 * @property string $Nome
 * @property string $Codigo
 * @property int $Valor
 * 
 * @property \App\Models\GbMgFE $gb_mg_f_e
 *
 * @package App\Models
 */
class GbFE extends Eloquent
{
	protected $table = 'gb_FE';
	protected $primaryKey = 'ID_FE';
	public $timestamps = false;

	protected $casts = [
		'Valor' => 'int'
	];

	protected $fillable = [
		'Nome',
		'Codigo',
		'Valor'
	];

	public function gb_mg_f_e()
	{
		return $this->hasOne(\App\Models\GbMgFE::class, 'ID_FE');
	}
}
