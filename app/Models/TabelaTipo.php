<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Jan 2017 17:34:55 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TabelaTipo
 * 
 * @property int $id
 * @property string $nome_tipo
 * @property int $codigo_tipo
 *
 * @package App\Models
 */
class TabelaTipo extends Eloquent
{
	protected $table = 'tabela_tipo';
	public $timestamps = false;

	protected $casts = [
		'codigo_tipo' => 'int'
	];

	protected $fillable = [
		'nome_tipo',
		'codigo_tipo'
	];
}
