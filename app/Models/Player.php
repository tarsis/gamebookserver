<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 18 Mar 2017 14:35:35 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GbPlayer
 * 
 * @property int $id
 * @property string $player_name
 * @property int $age
 * @property string $entity
 *
 * @package App\Models
 */
class Player extends Eloquent
{
	protected $table = 'player_bkp';
	public $timestamps = true;

	protected $casts = [
		'id' => 'int',
		'age' => 'int'
	];

	protected $fillable = [
		'player_name',
		'age',
		'entity',
        'created_at'
	];

	public function owner() {
        return $this->belongsTo(Player::class);
    }
}
