<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Jan 2017 17:34:55 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GamebookTest
 * 
 * @property int $id
 * @property string $nome
 * @property int $minigame
 * @property int $nivel
 * @property string $variavel
 * @property float $valor
 * @property float $max
 * @property string $data
 * @property string $hora
 *
 * @package App\Models
 */
class GamebookTest extends Eloquent
{
	protected $table = 'gamebookTest';
	public $timestamps = false;

	protected $casts = [
		'minigame' => 'int',
		'nivel' => 'int',
		'valor' => 'float',
		'max' => 'float'
	];

	protected $fillable = [
		'nome',
		'minigame',
		'nivel',
		'variavel',
		'valor',
		'max',
		'data',
		'hora'
	];
}
