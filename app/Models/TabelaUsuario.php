<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Jan 2017 17:34:55 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TabelaUsuario
 * 
 * @property int $id
 * @property string $login
 * @property string $senha
 * @property string $Primeiro_nome
 * @property string $email
 * @property int $quantidade
 *
 * @package App\Models
 */
class TabelaUsuario extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'quantidade' => 'int'
	];

	protected $fillable = [
		'login',
		'senha',
		'Primeiro_nome',
		'email',
		'quantidade'
	];
}
