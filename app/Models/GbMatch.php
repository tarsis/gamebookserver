<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 18 Mar 2017 14:35:35 -0300.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
/**
 * Class GbMatch
 * 
 * @property int $id
 * @property string $game_session_id
 * @property string $player_id
 * @property int $minigame_id
 * @property int $level_id
 * @property \Carbon\Carbon $datetime
 * @property int $hits
 * @property int $errors
 * @property int $repeated_errors
 * @property int $omissions
 * @property int $duration
 * @property \Carbon\Carbon $created_at
 *
 * @package App\Models
 */
class GbMatch extends Eloquent
{
	protected $table = 'match';
	public $timestamps = true;

	protected $casts = [
		'player_id' => 'int',
		'minigame_id' => 'int',
		'level_id' => 'int',
		'hits' => 'int',
		'errors' => 'int',
		'repeated_errors' => 'int',
		'omissions' => 'int',
		'duration' => 'int'
	];

	protected $dates = [
		'datetime'
	];

	protected $fillable = [
		'game_session_id',
		'player_id',
		'minigame_id',
		'level_id',
		'datetime',
		'hits',
		'errors',
		'repeated_errors',
		'omissions',
		'duration'
	];

    public function player() {
        return $this->belongsTo(GbPlayer::class, 'player_id');
    }

    public function events() {
        return $this->hasMany(GbEvent::class, 'match_id');
    }
}
