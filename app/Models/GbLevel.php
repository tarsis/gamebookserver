<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 18 Mar 2017 14:35:35 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GbLevel
 * 
 * @property int $id
 * @property int $minigame_id
 * @property int $level_number
 * 
 * @property \App\Models\GbMinigame $gb_minigame
 *
 * @package App\Models
 */
class GbLevel extends Eloquent
{
	protected $table = 'level';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'minigame_id' => 'int',
		'level_number' => 'int'
	];

	protected $fillable = [
		'id',
		'minigame_id',
		'level_number'
	];

	public function gb_minigame()
	{
		return $this->belongsTo(\App\Models\GbMinigame::class, 'minigame_id');
	}
}
