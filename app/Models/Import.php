<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Jan 2017 17:34:55 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GamebookMetrics
 *
 * @package App\Models
 */
class Import extends Eloquent
{
    CONST VAZIO = -1;
    CONST PARA_EXTRAIR = 0;
    CONST EXTRAIDO = 1;
    CONST DUPLICADO = 2;
    CONST JSON_INVALIDO = 3;
    CONST CORTADO = 5;

	protected $table = 'import';
	public $timestamps = false;

	protected $fillable = [
		'id',
		'payload',
        'extracted'
	];

	public function toExtract() {
	    return $this
            ->where('extracted','=', self::PARA_EXTRAIR)
            //->orWhere('extracted','>=',1)
            ->where('id','>=',1731)
            ->orderBy('id'); //->limit(100);
    }

    public function incomplete() {
        return $this
            ->where('extracted','=', self::CORTADO)
            //->orWhere('extracted','>=',1)
            //->where('id','>=',1350)
            ->orderBy('id');
    }

    public function payloadCortado() {
        return $this
            ->where('count(payload)','=', 65535)
            ->orderBy('id');
    }
}
