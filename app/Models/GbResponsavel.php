<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 18 Mar 2017 14:35:35 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GbResponsavel
 * 
 * @property int $ID_Responsavel
 * @property string $Nome
 * @property string $Instituicao
 *
 * @package App\Models
 */
class GbResponsavel extends Eloquent
{
	protected $table = 'gb_responsavel';
	protected $primaryKey = 'ID_Responsavel';
	public $timestamps = false;

	protected $fillable = [
		'Nome',
		'Instituicao'
	];
}
