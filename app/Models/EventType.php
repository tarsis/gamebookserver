<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 18 Mar 2017 14:35:35 -0300.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class EventType
 * 
 * @property int $id
 * @property int $name
 * @property int $description
 *
 * @package App\Models
 */
class EventType extends Eloquent
{
	protected $table = 'event_type';
	public $timestamps = false;
}
