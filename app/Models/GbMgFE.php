<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Jan 2017 17:34:55 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GbMgFE
 * 
 * @property int $ID_Minigame
 * @property int $ID_FE
 * 
 * @property \App\Models\GbMinigame $gb_minigame
 * @property \App\Models\GbFE $gb_f_e
 *
 * @package App\Models
 */
class GbMgFE extends Eloquent
{
	protected $table = 'gb_MgFE';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'ID_Minigame' => 'int',
		'ID_FE' => 'int'
	];

	protected $fillable = [
		'ID_Minigame',
		'ID_FE'
	];

	public function gb_minigame()
	{
		return $this->belongsTo(\App\Models\GbMinigame::class, 'ID_Minigame');
	}

	public function gb_f_e()
	{
		return $this->belongsTo(\App\Models\GbFE::class, 'ID_FE');
	}
}
