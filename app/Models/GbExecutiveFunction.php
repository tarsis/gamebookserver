<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 18 Mar 2017 14:35:35 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GbExecutiveFunction
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 *
 * @package App\Models
 */
class GbExecutiveFunction extends Eloquent
{
	protected $table = 'executive_function';
	public $timestamps = false;

	protected $fillable = [
		'name',
		'description'
	];
}
