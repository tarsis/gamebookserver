<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 18 Mar 2017 14:35:35 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Gamebook
 * 
 * @property int $id
 * @property string $nome
 * @property int $minigame
 * @property int $nivel
 * @property string $variavel
 * @property float $valor
 * @property float $max
 * @property string $data
 * @property string $hora
 *
 * @package App\Models
 */
class Gamebook extends Eloquent
{
	protected $table = 'gamebook';
	public $timestamps = false;

	protected $casts = [
		'minigame' => 'int',
		'nivel' => 'int',
		'valor' => 'float',
		'max' => 'float'
	];

	protected $fillable = [
		'nome',
		'minigame',
		'nivel',
		'variavel',
		'valor',
		'max',
		'data',
		'hora'
	];
}
