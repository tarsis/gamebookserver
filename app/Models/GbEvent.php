<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 18 Mar 2017 14:35:35 -0300.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class GbEvent
 * 
 * @property int $id
 * @property int $match_id
 * @property int $category
 * @property int $type
 * @property string $name
 * @property string $click_target
 * @property string $drop_target
 * @property float $target_x
 * @property float $target_y
 *
 * @package App\Models
 */
class GbEvent extends Eloquent
{

    //eventos de interacao
    const TYPE_HIT = 1;
    const TYPE_ERROR = 2;
    const TYPE_ALEATORY_ACTION = 3;
    const TYPE_MOVE = 4;


    //eventos do jogo
    const TYPE_GAME_START = 10;
    const TYPE_GAME_FINISH = 12;
    const TYPE_GAME_OVER = 13;
    const TYPE_GAME_PAUSE = 14;
    const TYPE_GAME_RESUME = 15;

    const TYPE_GAME_OMISSION = 11;
    const TYPE_GAME_SHOWED_TARGET = 16;

	protected $table = 'event';
	public $timestamps = true;

	protected $casts = [
		'match_id' => 'int',
		'category' => 'int',
		'type' => 'int',
		'target_x' => 'float',
		'target_y' => 'float'
	];

	protected $dates = [
	//	'datetime'
	];

	protected $fillable = [
		'match_id',
		'category',
		'type',
		'name',
		'datetime',
		'click_target',
		'drop_target',
		'target_x',
		'target_y',
        'created_at',
        'second_int'
	];

    public function match() {
        return $this->belongsTo(Match::class,'match_id', 'id');
    }

    public function getTargetName() {
        return explode(' ', $this->click_target)[0];
    }

    public function getTargetSize()
    {
        $target  = strtolower($this->click_target);
        if(false !== strpos($target, "pequen")) {
            return 1;
        };
        if(false !== strpos($target, "med")) {
            return 2;
        };
        if(false !== strpos($target, "grand")) {
            return 3;
        };
        return 4;
    }

}
