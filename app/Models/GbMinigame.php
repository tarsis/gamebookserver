<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 18 Mar 2017 14:35:35 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GbMinigame
 * 
 * @property int $id
 * @property string $name
 * @property int $game_number
 * @property int $events_per_move
 * @property int $perfect_move
 * @property string $description
 * 
 * @property \App\Models\GbLevel $gb_level
 *
 * @package App\Models
 */
class GbMinigame extends Eloquent
{
	protected $table = 'minigame';
	public $timestamps = false;

	protected $casts = [
		'game_number' => 'int',
		'events_per_move' => 'int',
		'perfect_move' => 'int'
	];

	protected $fillable = [
		'name',
		'game_number',
		'events_per_move',
		'perfect_move',
		'description'
	];

	public function gb_level()
	{
		return $this->hasOne(\App\Models\GbLevel::class, 'minigame_id');
	}
}
