<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 18 Mar 2017 14:35:35 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class GamebookMetric
 * 
 * @property int $id
 * @property string $json
 *
 * @package App\Models
 */
class GamebookMetric extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'json'
	];
}
